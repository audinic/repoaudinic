package ni.com.audinic.reports;

public enum FormatoReporte {
    PDF("pdf"), EXCEL("xls");

    private final String formato;

    private FormatoReporte(final String formato) {
        this.formato = formato;
    }

    public String getCodigo() {
        return this.formato;
    }

    public static FormatoReporte getPorCodigo(final String codigo) {
        for (FormatoReporte e : FormatoReporte.values()) {
            if (e.getCodigo().equals(codigo)) {
                return e;
            }
        }
        throw new IllegalArgumentException("El codigo para FormatoReporte no existe.");
    }
}