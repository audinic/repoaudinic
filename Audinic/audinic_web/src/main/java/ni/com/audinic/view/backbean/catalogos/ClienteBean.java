package ni.com.audinic.view.backbean.catalogos;

import ni.com.audinic.businesslogic.service.ClienteService;
import ni.com.audinic.businesslogic.service.DepartamentoService;
import ni.com.audinic.businesslogic.service.DetalleCatalogoService;
import ni.com.audinic.businesslogic.service.MunicipioService;
import ni.com.audinic.model.catalogos.Cliente;
import ni.com.audinic.model.catalogos.Departamento;
import ni.com.audinic.model.catalogos.DetalleCatalogo;
import ni.com.audinic.model.catalogos.Municipio;
import ni.com.audinic.model.catalogos.TipoCatalogos;
import ni.com.audinic.view.utils.JsfUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Named;

import java.util.Date;
import java.util.ArrayList;
import java.util.List;

/**
 * Creado por fmgarcia el 23 / 06 / 2017.
 */
@Named
@Scope("view")
public class ClienteBean extends JsfUtils {

    private List<Cliente> listaClientes;
    private Cliente clienteSelected;

    private String numeroIdentificacion;

    private List<DetalleCatalogo> listaCategoria;
    private String categoriaSelected;

    private String nss;

    private List<DetalleCatalogo> listaSexo;
    private String sexoSelected;

    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private Date fechaNacimiento;
    private String telefono;
    private String correo;

    private List<Departamento> listaDepartamento;
    private String departamentoSelected;
    private List<Municipio> listaMunicipio;
    private String municipioSelected;

    private String direccionDomiciliar;
    private Boolean activo;

    // Servicios
    private final ClienteService clienteService;
    private final DetalleCatalogoService detalleCatalogoService;
    private final DepartamentoService departamentoService;
    private final MunicipioService municipoService;

    @PostConstruct
    public void init() {
        // inicializamos las listas
        this.listaClientes = new ArrayList<>();
        this.listaCategoria = new ArrayList<>();
        this.listaSexo = new ArrayList<>();
        this.listaDepartamento = new ArrayList<>();
        this.listaMunicipio = new ArrayList<>();
        this.cargarClientes();
    }

    @Autowired
    public ClienteBean(final ClienteService clienteService, final DetalleCatalogoService detalleCatalogoService,
            final DepartamentoService departamentoService, final MunicipioService municipoService) {

        this.clienteService = clienteService;
        this.detalleCatalogoService = detalleCatalogoService;
        this.departamentoService = departamentoService;
        this.municipoService = municipoService;
    }

    public void limpiarDatos() {
        this.clienteSelected = null;
        this.numeroIdentificacion = null;
        this.categoriaSelected = null;
        this.nss = null;
        this.sexoSelected = null;
        this.primerNombre = null;
        this.segundoNombre = null;
        this.primerApellido = null;
        this.segundoApellido = null;
        this.fechaNacimiento = null;
        this.telefono = null;
        this.correo = null;
        this.departamentoSelected = null;
        this.municipioSelected = null;
        this.listaMunicipio.clear();
        this.direccionDomiciliar = null;
        this.activo = true;
    }

    public void nuevoCliente() {
        this.cargarCatalogos();
        this.limpiarDatos();
    }

    public void onClienteSelected() {
        this.cargarCatalogos();
        if (this.clienteSelected != null) {
            this.numeroIdentificacion = clienteSelected.getNumeroIdentificacion();
            this.categoriaSelected = clienteSelected.getCategoria().getCodigo();
            this.nss = clienteSelected.getNss();
            this.sexoSelected = clienteSelected.getSexo().getCodigo();
            this.primerNombre = clienteSelected.getPrimerNombre();
            this.segundoNombre = clienteSelected.getSegundoNombre();
            this.primerApellido = clienteSelected.getPrimerApellido();
            this.segundoApellido = clienteSelected.getSegundoApellido();
            this.fechaNacimiento = clienteSelected.getFechaNacimiento();
            this.telefono = clienteSelected.getTelefono();
            this.correo = clienteSelected.getCorreo();

            if (this.clienteSelected.getMunicipio() != null) {
                this.departamentoSelected = clienteSelected.getMunicipio().getDepartamento().getCodigo();
                this.obtenerMunicipios();
                this.municipioSelected = clienteSelected.getMunicipio().getCodigo();
            } else {
                this.departamentoSelected = null;
                this.municipioSelected = null;
            }
            
            this.direccionDomiciliar = clienteSelected.getDireccionDomiciliar();
            this.activo = clienteSelected.getActivo();
        }
    }

    private void cargarClientes() {
        listaClientes = this.clienteService.listarClientes();
    }

    private void cargarCatalogos() {
        listaCategoria = detalleCatalogoService.listarPorCodigo(TipoCatalogos.CATEGORIA_CLIENTE.getCodigo());
        listaSexo = detalleCatalogoService.listarPorCodigo(TipoCatalogos.SEXO.getCodigo());
        listaDepartamento = departamentoService.listarDepartamentos();
        // listaMunicipio = municipoService.listarMunicipiosPorDepartamento();
    }

    public void obtenerMunicipios() {
        this.listaMunicipio.clear();
        this.listaMunicipio = municipoService.listarMunicipiosPorDepartamento(this.departamentoSelected);
    }

    public void gestionClientes() {
        try {

            Cliente cliente = new Cliente();

            cliente.setNumeroIdentificacion(this.numeroIdentificacion);
            cliente.setCategoria(this.detalleCatalogoService.encontrarPorCodigo(this.categoriaSelected));
            cliente.setNss(this.nss);
            cliente.setSexo(this.detalleCatalogoService.encontrarPorCodigo(this.sexoSelected));
            cliente.setPrimerNombre(this.primerNombre);
            cliente.setSegundoNombre(this.segundoNombre);
            cliente.setPrimerApellido(this.primerApellido);
            cliente.setSegundoApellido(this.segundoApellido);
            cliente.setFechaNacimiento(this.fechaNacimiento);
            cliente.setTelefono(this.telefono);
            cliente.setCorreo(this.correo);

            if (this.municipioSelected != null) {
                cliente.setMunicipio(this.municipoService.obtenerMunicipioPorCodigo(this.municipioSelected));
            }

            cliente.setDireccionDomiciliar(this.direccionDomiciliar);
            cliente.setActivo(this.activo);

            if (this.clienteSelected != null) {
                cliente.setId(clienteSelected.getId());
                this.clienteService.modificarCliente(cliente);

                mostrarMensajeSuccess("Cliente modficado correctamente");
            } else {
                this.clienteService.agregarCliente(cliente);
                mostrarMensajeSuccess("Cliente agregado correctamente");
            }

            this.cargarClientes();

        } catch (Exception e) {
            mostrarMensajeError(this.getClass().getSimpleName(), "gestionClientes", "error al guardar registro", e);
        }
    }

    public List<Cliente> getListaClientes() {
        return listaClientes;
    }

    public void setListaClientes(List<Cliente> listaClientes) {
        this.listaClientes = listaClientes;
    }

    public Cliente getClienteSelected() {
        return clienteSelected;
    }

    public void setClienteSelected(Cliente clienteSelected) {
        this.clienteSelected = clienteSelected;
    }

    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    public List<DetalleCatalogo> getListaCategoria() {
        return listaCategoria;
    }

    public void setListaCategoria(List<DetalleCatalogo> listaCategoria) {
        this.listaCategoria = listaCategoria;
    }

    public String getCategoriaSelected() {
        return categoriaSelected;
    }

    public void setCategoriaSelected(String categoriaSelected) {
        this.categoriaSelected = categoriaSelected;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public List<DetalleCatalogo> getListaSexo() {
        return listaSexo;
    }

    public void setListaSexo(List<DetalleCatalogo> listaSexo) {
        this.listaSexo = listaSexo;
    }

    public String getSexoSelected() {
        return sexoSelected;
    }

    public void setSexoSelected(String sexoSelected) {
        this.sexoSelected = sexoSelected;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public List<Departamento> getListaDepartamento() {
        return listaDepartamento;
    }

    public void setListaDepartamento(List<Departamento> listaDepartamento) {
        this.listaDepartamento = listaDepartamento;
    }

    public String getDepartamentoSelected() {
        return departamentoSelected;
    }

    public void setDepartamentoSelected(String departamentoSelected) {
        this.departamentoSelected = departamentoSelected;
    }

    public List<Municipio> getListaMunicipio() {
        return listaMunicipio;
    }

    public void setListaMunicipio(List<Municipio> listaMunicipio) {
        this.listaMunicipio = listaMunicipio;
    }

    public String getMunicipioSelected() {
        return municipioSelected;
    }

    public void setMunicipioSelected(String municipioSelected) {
        this.municipioSelected = municipioSelected;
    }

    public String getDireccionDomiciliar() {
        return direccionDomiciliar;
    }

    public void setDireccionDomiciliar(String direccionDomiciliar) {
        this.direccionDomiciliar = direccionDomiciliar;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

}
