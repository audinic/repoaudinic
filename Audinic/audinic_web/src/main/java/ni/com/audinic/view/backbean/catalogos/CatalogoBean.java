package ni.com.audinic.view.backbean.catalogos;

import ni.com.audinic.businesslogic.service.CatalogoService;
import ni.com.audinic.businesslogic.service.DetalleCatalogoService;
import ni.com.audinic.model.catalogos.Catalogo;
import ni.com.audinic.model.catalogos.DetalleCatalogo;
import ni.com.audinic.view.utils.JsfUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Creado por fmgarcia el 20 / 06 / 2017.
 */
@Named
@Scope("view")
public class CatalogoBean extends JsfUtils {

    private String filtro;
    private String codigo;
    private String nombre;
    private String descripcion;
    private Boolean activo=true;
    private List<Catalogo> listaCatalogos;
    private Catalogo catalogoSelected;
    private List<DetalleCatalogo> listaDetalleCatalogo;
    private DetalleCatalogo detalleCatalogoSelected;
    private DetalleCatalogo detalleCatalogo;
    private boolean mostrarBotonGuardar=false;


    /*Servicios*/
    private final CatalogoService catalogoService;
    private final DetalleCatalogoService detalleCatalogoService;

    @PostConstruct
    public void init(){
        this.inicializarDatos();
        this.cargarCatalogos();
    }

    @Autowired
    public CatalogoBean(final CatalogoService catalogoService, DetalleCatalogoService detalleCatalogoService) {
        this.catalogoService=catalogoService;
        this.detalleCatalogoService=detalleCatalogoService;

    }

    /*METODOS PRIVADOS*/

    private void inicializarListas(){
        this.listaDetalleCatalogo=new ArrayList<>();
    }
    private void inicializarDatos(){
        this.catalogoSelected=null;
        this.codigo="";
        this.nombre="";
        this.descripcion="";
        this.limpiarValorDialogo();
    }

    private void cargarCatalogos(){
        try {
            this.listaCatalogos = catalogoService.listarCatalogos(null);
        }catch (Exception e){
            mostrarMensajeError(this.getClass().getSimpleName(),"Error al cargar los datos",e.getMessage(),e);
        }
    }
    private void obtenerValores(){
        if (this.catalogoSelected != null)
        {
            this.codigo=this.catalogoSelected.getCodigo();
            this.nombre=this.catalogoSelected.getNombre();
            this.descripcion=this.catalogoSelected.getDescripcion();
            this.activo= this.catalogoSelected.getActivo();
            this.listaDetalleCatalogo = detalleCatalogoService.listarPorCodigo(this.catalogoSelected.getCodigo());
        }
    }

    /*METODOS PUBLICOS*/
    @Transactional
    public void gestionCatalogos(){
        try {
            if (this.catalogoSelected == null) {
                this.catalogoSelected = new Catalogo();
                this.catalogoSelected.setCodigo(this.codigo);
                this.catalogoSelected.setNombre(this.nombre);
                this.catalogoSelected.setDescripcion(this.descripcion);
                this.catalogoSelected.setActivo(this.activo);
                this.catalogoService.agregar(catalogoSelected);

            } else {
                this.catalogoSelected.setCodigo(this.codigo);
                this.catalogoSelected.setNombre(this.nombre);
                this.catalogoSelected.setDescripcion(this.descripcion);
                this.catalogoSelected.setActivo(this.activo);
                this.catalogoService.modificar(catalogoSelected);
            }

            if (!listaDetalleCatalogo.isEmpty()) {
                for (DetalleCatalogo detalle : this.listaDetalleCatalogo) {
                    detalle.setCatalogo(catalogoSelected);
                    DetalleCatalogo temporal = detalleCatalogoService.encontrarPorCodigo(catalogoSelected.getCodigo(), detalle.getCodigo());
                    if (temporal != null) {
                        detalleCatalogoService.modificar(detalle);
                    } else {

                        detalleCatalogoService.agregar(detalle);
                    }
                }
            }


            this.inicializarDatos();
            this.inicializarListas();
            this.cargarCatalogos();
            mostrarMensajeSuccess("Datos guardados correctamente");
        }catch (Exception e){
            mostrarMensajeError(this.getClass().getSimpleName(),"gestionCatalogos()",e.getMessage(),e);
        }

    }

    public void onValorCatalogoSelected()
    {

        if (this.detalleCatalogoSelected != null)
        {
            detalleCatalogo=detalleCatalogoSelected;
        }
    }
    public void onCatalogoSelected(){
        this.mostrarBotonGuardar=true;
        obtenerValores();
        limpiarValorDialogo();
    }
    public void limpiarValorDialogo(){
        detalleCatalogo= new DetalleCatalogo();
        //por defecto va ser activo
        detalleCatalogo.setActivo(true);
    }
    public void agregarDetalleCatalogo(){

        if (detalleCatalogo!=null){
            this.listaDetalleCatalogo.add(detalleCatalogo);
        }
    }
    public void nuevoCatalogo(){
        this.mostrarBotonGuardar=true;
        this.inicializarDatos();
        this.inicializarListas();
    }
    public void buscar(){
        this.listaCatalogos = catalogoService.listarCatalogos(this.filtro);
    }


    /*GETTERS Y SETTERS*/

    public List<Catalogo> getListaCatalogos() {
        return listaCatalogos;
    }

    public void setListaCatalogos(List<Catalogo> listaCatalogos) {
        this.listaCatalogos = listaCatalogos;
    }

    public Catalogo getCatalogoSelected() {
        return catalogoSelected;
    }

    public void setCatalogoSelected(Catalogo catalogoSelected) {
        this.catalogoSelected = catalogoSelected;
    }

    public List<DetalleCatalogo> getListaDetalleCatalogo() {
        return listaDetalleCatalogo;
    }

    public void setListaDetalleCatalogo(List<DetalleCatalogo> listaDetalleCatalogo) {
        this.listaDetalleCatalogo = listaDetalleCatalogo;
    }

    public DetalleCatalogo getDetalleCatalogoSelected() {
        return detalleCatalogoSelected;
    }

    public void setDetalleCatalogoSelected(DetalleCatalogo detalleCatalogoSelected) {
        this.detalleCatalogoSelected = detalleCatalogoSelected;
    }

    public String getFiltro() {
        return filtro;
    }

    public void setFiltro(String filtro) {
        this.filtro = filtro;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    public DetalleCatalogo getDetalleCatalogo() {
        return detalleCatalogo;
    }

    public void setDetalleCatalogo(DetalleCatalogo detalleCatalogo) {
        this.detalleCatalogo = detalleCatalogo;
    }

    public boolean isMostrarBotonGuardar() {
        return mostrarBotonGuardar;
    }

    public void setMostrarBotonGuardar(boolean mostrarBotonGuardar) {
        this.mostrarBotonGuardar = mostrarBotonGuardar;
    }
}
