package ni.com.audinic.view.backbean.catalogos;

import ni.com.audinic.businesslogic.service.DetalleCatalogoService;
import ni.com.audinic.businesslogic.service.EquipoService;
import ni.com.audinic.model.catalogos.DetalleCatalogo;
import ni.com.audinic.model.catalogos.Equipos;
import ni.com.audinic.model.catalogos.TipoCatalogos;
import ni.com.audinic.view.utils.JsfUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Creado por fmgarcia el 23 / 06 / 2017.
 */
@Named
@Scope("view")
public class EquiposBean extends JsfUtils{

    private List<Equipos> listaEquipos;
    private Equipos equipoSelected;
    private List<DetalleCatalogo> listaTipoProtesis;
    private List<DetalleCatalogo> listaModelos;
    private List<DetalleCatalogo> listaTipoBateria;
    private String tipoProtesisSelected;
    private String modeloEquiposSelected;
    private String tipoBateriaSelected;
    private Integer numeroBaterias;
    private String descripcion;


    //Servicios
    private final EquipoService equipoService;
    private final DetalleCatalogoService detalleCatalogoService;


    @PostConstruct
    public void init(){
        //inicializamos las listas
        this.listaEquipos=new ArrayList<>();
        this.listaTipoBateria=new ArrayList<>();
        this.listaTipoProtesis=new ArrayList<>();
        this.listaModelos=new ArrayList<>();
        this.cargarEquipos();
    }

    @Autowired
    public EquiposBean(final EquipoService equipoService,
                       final DetalleCatalogoService detalleCatalogoService) {

        this.equipoService=equipoService;
        this.detalleCatalogoService=detalleCatalogoService;
    }
    public void limpiarDatos(){
        this.tipoProtesisSelected=null;
        this.modeloEquiposSelected=null;
        this.tipoBateriaSelected=null;
        this.numeroBaterias=null;
        this.descripcion=null;
    }

    public void nuevoEquipo(){
        this.cargarCatalogos();
        this.limpiarDatos();
    }
    public void onEquipoSelected(){
        this.cargarCatalogos();
        if (equipoSelected!=null){
            this.tipoProtesisSelected=equipoSelected.getTipoProtesis().getCodigo();
            this.modeloEquiposSelected=equipoSelected.getModelo().getCodigo();
            if (equipoSelected.getTipoBateria()!=null) {
                this.tipoBateriaSelected = equipoSelected.getTipoBateria().getCodigo();
            }
            this.numeroBaterias=equipoSelected.getNumeroBaterias();
            this.descripcion=equipoSelected.getDescripcion();
        }
    }

    private void cargarEquipos(){
        listaEquipos= this.equipoService.listarEquipos();
    }
    private void cargarCatalogos(){
        listaTipoProtesis= detalleCatalogoService.listarPorCodigo(TipoCatalogos.TIPO_PROTESIS.getCodigo());
        listaModelos=detalleCatalogoService.listarPorCodigo(TipoCatalogos.MODELO_EQUIPO.getCodigo());
        listaTipoBateria=detalleCatalogoService.listarPorCodigo(TipoCatalogos.TIPO_BATERIA.getCodigo());
    }
    public void gestionEquipos(){
        try {

            Equipos equipo= new Equipos();
            if (equipoSelected!=null){
                equipo.setId(equipoSelected.getId());
                equipo.setTipoProtesis(this.detalleCatalogoService.encontrarPorCodigo(this.tipoProtesisSelected));
                if (this.tipoBateriaSelected!=null) {
                    equipo.setTipoBateria(this.detalleCatalogoService.encontrarPorCodigo(this.tipoBateriaSelected));
                }
                equipo.setModelo(this.detalleCatalogoService.encontrarPorCodigo(this.modeloEquiposSelected));
                equipo.setNumeroBaterias(this.numeroBaterias);
                equipo.setDescripcion(this.descripcion);

                this.equipoService.modificarEquipo(equipo);

                mostrarMensajeSuccess("Equipo modficado correctamente");

            }else{
                equipo.setTipoProtesis(this.detalleCatalogoService.encontrarPorCodigo(this.tipoProtesisSelected));
                if (this.tipoBateriaSelected!=null) {
                    equipo.setTipoBateria(this.detalleCatalogoService.encontrarPorCodigo(this.tipoBateriaSelected));
                }

                equipo.setModelo(this.detalleCatalogoService.encontrarPorCodigo(this.modeloEquiposSelected));
                equipo.setNumeroBaterias(this.numeroBaterias);
                equipo.setDescripcion(this.descripcion);

                this.equipoService.agregarEquipo(equipo);

                mostrarMensajeSuccess("Equipo agregado correctamente");
            }
            this.cargarEquipos();

        }catch (Exception e){
            mostrarMensajeError(this.getClass().getSimpleName(),"gestionEquipos","error al guardar registro",e);
        }
    }


    public List<Equipos> getListaEquipos() {
        return listaEquipos;
    }

    public void setListaEquipos(List<Equipos> listaEquipos) {
        this.listaEquipos = listaEquipos;
    }

    public Equipos getEquipoSelected() {
        return equipoSelected;
    }

    public void setEquipoSelected(Equipos equipoSelected) {
        this.equipoSelected = equipoSelected;
    }

    public List<DetalleCatalogo> getListaTipoProtesis() {
        return listaTipoProtesis;
    }

    public void setListaTipoProtesis(List<DetalleCatalogo> listaTipoProtesis) {
        this.listaTipoProtesis = listaTipoProtesis;
    }

    public List<DetalleCatalogo> getListaModelos() {
        return listaModelos;
    }

    public void setListaModelos(List<DetalleCatalogo> listaModelos) {
        this.listaModelos = listaModelos;
    }

    public List<DetalleCatalogo> getListaTipoBateria() {
        return listaTipoBateria;
    }

    public void setListaTipoBateria(List<DetalleCatalogo> listaTipoBateria) {
        this.listaTipoBateria = listaTipoBateria;
    }

    public String getTipoProtesisSelected() {
        return tipoProtesisSelected;
    }

    public void setTipoProtesisSelected(String tipoProtesisSelected) {
        this.tipoProtesisSelected = tipoProtesisSelected;
    }

    public String getModeloEquiposSelected() {
        return modeloEquiposSelected;
    }

    public void setModeloEquiposSelected(String modeloEquiposSelected) {
        this.modeloEquiposSelected = modeloEquiposSelected;
    }

    public String getTipoBateriaSelected() {
        return tipoBateriaSelected;
    }

    public void setTipoBateriaSelected(String tipoBateriaSelected) {
        this.tipoBateriaSelected = tipoBateriaSelected;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getNumeroBaterias() {
        return numeroBaterias;
    }

    public void setNumeroBaterias(Integer numeroBaterias) {
        this.numeroBaterias = numeroBaterias;
    }
}
