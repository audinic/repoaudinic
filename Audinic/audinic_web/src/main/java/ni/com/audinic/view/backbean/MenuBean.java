package ni.com.audinic.view.backbean;

import ni.com.audinic.businesslogic.service.MenuService;
import ni.com.audinic.model.seguridad.Menu;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.menu.DefaultMenuItem;
import org.primefaces.model.menu.DefaultMenuModel;
import org.primefaces.model.menu.DefaultSubMenu;
import org.primefaces.model.menu.MenuModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.faces.application.ViewHandler;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.Servlet;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Creado por fmgarcia el 13 / 06 / 2017.
 */
@Named
@Scope("view")
public class MenuBean {

    private String paginaActual;
    private String htmlMenu;
    private final MenuService menuService;
    private List<Menu> listaMenu;
    private String ulMenu="<ul id=\'layout-menu\' class=\'BordRad3 Unselectable\' tabindex=\'0\'>%s</ul>";
    private String ulSubMenu="<ul class=\'submenu\' role=\'menu\'>%s</ul>";
    private String ulSubMenuhijo="<ul class=\'submenu\' role=\'menu\'>%s</ul>";
    private String liMenu="<li class=\'menuitem\' role=\'menuitem\' ><a class=\'Animated05 CursPointer\' onclick=\'%s\'  tabindex=\'-1\'> %s &nbsp;<i class=\'fa fa-chevron-down Fs12 Fright\'></i></a>%s</li>";
    private String liSubMenu="<li class=\'menuitem\' role=\'menuitem\' ><a class=\'Animated05 CursPointer\'  onclick=\'%s\'> %s</a>%s</li>";
    @PostConstruct
    public void init(){
        this.paginaActual="";

        listaMenu= new ArrayList<>();
        htmlMenu= this.crearMenu();
    }

    @Autowired
    public MenuBean(final MenuService menuService) {
        this.menuService=menuService;
    }

    @SuppressWarnings("unchecked")
    private String crearMenu(){
        String patron="%hj";
        String sMenu="";


        listaMenu= menuService.listarMenus();
        List<Menu> hijos;
        Iterator<Menu> iterador= listaMenu.iterator();

        while (iterador.hasNext()){
            Menu m= iterador.next();
            hijos=this.tieneHijos(m);
            if (hijos.size()!=0) {
                if(StringUtils.contains(sMenu,patron+ m.getId().toString())){
                   sMenu= StringUtils.replace(sMenu, patron+m.getId().toString(), this.crearSubMenu(patron,hijos));
                }else {
                    if(m.getUrl()==null){
                        sMenu += String.format(liMenu, "return false;", m.getNombre(), this.crearSubMenu(patron, hijos));
                    }else {
                        sMenu += String.format(liMenu, "goToUrl("+ m.getId()+")", m.getNombre(), this.crearSubMenu(patron, hijos));
                    }
                }

            }
            //iterador.remove();
        }
        for(Menu mn: listaMenu){
            sMenu= StringUtils.replace(sMenu, patron+mn.getId().toString(),"");
        }

        return String.format(ulMenu,sMenu);
    }
    private String crearSubMenu(String patron,List<Menu> subMenus){
        String smenu="";
        String icono="<i class=\'fa fa-chevron-down Fs12 Fright\'></i> ";
        for(Menu menu: subMenus){
            List<Menu> hijos=this.tieneHijos(menu);
            if (hijos.size()!=0) {
                if (menu.getUrl() == null) {
                    smenu += String.format(this.liSubMenu, "return false;",  icono + menu.getNombre(), patron + menu.getId().toString());
                } else {
                    smenu += String.format(this.liSubMenu, "goToUrl(" + menu.getId() + ")", menu.getNombre(), patron + menu.getId().toString());
                }
            }else{
                if (menu.getUrl() == null) {
                    //smenu += String.format(this.liSubMenu, "return false;", menu.getNombre() , patron + menu.getId().toString());
                    smenu += String.format(this.liSubMenu, "return false;", menu.getNombre() , "");
                } else {
                    //smenu += String.format(this.liSubMenu, "goToUrl(" + menu.getId() + ")", menu.getNombre(), patron + menu.getId().toString());
                    smenu += String.format(this.liSubMenu, "goToUrl(" + menu.getId() + ")", menu.getNombre(), "");
                }
            }
        }

        return String.format(this.ulSubMenu, smenu);
    }
    private List<Menu> tieneHijos(Menu menu){
        List<Menu> local= listaMenu.stream().filter(menus-> menus.getPadre()!=null).collect(Collectors.toList());
       return local.stream().sorted((e1, e2) -> Integer.compare(e1.getOrden(),
               e2.getOrden())).filter(menus -> menus.getPadre().equals(menu)).collect(Collectors.toList());
    }

    public void visitarUrl(){
        Map requestParamMap = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
        String idMenu = (String)requestParamMap.get("url");
        if (!idMenu.isEmpty()) {
            this.paginaActual = menuService.encontrarPorId(Integer.parseInt(idMenu)).getUrl();
        }
    }

    public String getHtmlMenu() {
        return htmlMenu;
    }

    public void setHtmlMenu(String htmlMenu) {
        this.htmlMenu = htmlMenu;
    }

    public String getPaginaActual() {
        return paginaActual;
    }

    public void setPaginaActual(String paginaActual) {
        this.paginaActual = paginaActual;
    }
}
