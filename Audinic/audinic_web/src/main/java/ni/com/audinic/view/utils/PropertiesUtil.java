package ni.com.audinic.view.utils;

import java.io.IOException;
import java.util.Properties;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.support.PropertiesLoaderUtils;

public class PropertiesUtil {
    private final Properties properties;
    private static final PropertiesUtil instance = new PropertiesUtil();

    private PropertiesUtil() {
        if (instance == null) {
            try {
                this.properties = PropertiesLoaderUtils.loadProperties(new ClassPathResource("config.properties"));
            } catch (IOException e) {
                throw new IllegalStateException(e);
            }
        } else {
            throw new IllegalStateException("Este constructor no puede ser llamado de nuevo.");
        }
    }

    public static Properties getProperties() {
        return instance.properties;
    }
}
