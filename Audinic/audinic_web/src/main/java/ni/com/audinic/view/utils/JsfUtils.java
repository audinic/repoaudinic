package ni.com.audinic.view.utils;

import org.apache.log4j.Logger;

import org.hibernate.validator.internal.engine.ConstraintViolationImpl;
import javax.validation.ConstraintViolationException;

/**
 * Creado por fmgarcia el 20 / 06 / 2017.
 */
public class JsfUtils {

    private static Logger logger = Logger.getLogger(JsfUtils.class);

    public void mostrarMensajeError(String targetErrorClassName, String targetErrorMethodName, String mensaje, Exception e) {
        FacesUtils.addErrorMessage(mensaje + e.getMessage());

        logger.error(targetErrorClassName + "#" + targetErrorMethodName, e);
    }

    public void mostrarMensajeError(String mensaje) {
        FacesUtils.addErrorMessage(mensaje);
    }

    public void mostrarMensajeInfo(String mensaje) {
        FacesUtils.addInfoMessage(mensaje);
    }

    public void mostrarMensajeSuccess(String mensaje) {
        FacesUtils.addSuccessMessage(mensaje);
    }

    public void mostrarMensajeException(Exception e) {
        if(e.getClass().getName().equals("javax.validation.ConstraintViolationException")) {
            this.mostrarMensajeError(((ConstraintViolationImpl)((ConstraintViolationException)e).getConstraintViolations().toArray()[0]).getMessageTemplate());
        } else {
            this.mostrarMensajeError(e.getMessage());
        }

    }

}

