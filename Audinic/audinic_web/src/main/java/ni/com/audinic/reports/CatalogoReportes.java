package ni.com.audinic.reports;

import ni.com.audinic.view.utils.PropertiesUtil;

public enum CatalogoReportes
{
    ConsolidadoFacturaCompraProveedor(
            "reportes/rpt_test",
            "Test");

    private final String nombre;
    private final String jasperPath;
    private final String reportPath;

    private CatalogoReportes(final String reportPath, final String nombre) {
        this.jasperPath = PropertiesUtil.getProperties().getProperty("jasperserver.reportsPath");
        this.nombre = nombre;
        this.reportPath = reportPath;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getReportPath() {
        return this.reportPath;
    }

    public String getFullPath() {
        return this.jasperPath + reportPath;
    }
}
