package ni.com.audinic.view.backbean.catalogos;

import ni.com.audinic.businesslogic.service.DetalleCatalogoService;
import ni.com.audinic.businesslogic.service.ServicioService;
import ni.com.audinic.model.catalogos.DetalleCatalogo;
import ni.com.audinic.model.catalogos.Servicio;
import ni.com.audinic.model.catalogos.TipoCatalogos;
import ni.com.audinic.view.utils.JsfUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Creado por fmgarcia el 23 / 06 / 2017.
 */
@Named
@Scope("view")
public class ServiciosBean extends JsfUtils{

    private List<Servicio> listaServicios;
    private Servicio servicioSelected;
    private List<DetalleCatalogo> listaTipoServicio;
    private String tipoServicioSelected;
    private String nombre;
    private String descripcion;
    private BigDecimal precio;


    //Servicios
    private final ServicioService servicioService;
    private final DetalleCatalogoService detalleCatalogoService;


    @PostConstruct
    public void init(){
        //inicializamos las listas
        this.listaServicios=new ArrayList<>();
        this.cargarServicios();
    }

    @Autowired
    public ServiciosBean(final ServicioService servicioService,
                       final DetalleCatalogoService detalleCatalogoService) {

        this.servicioService=servicioService;
        this.detalleCatalogoService=detalleCatalogoService;
    }
    public void limpiarDatos(){
        this.tipoServicioSelected=null;
        this.nombre=null;
        this.precio=null;
        this.descripcion=null;
    }

    public void nuevoServicio(){
        this.cargarCatalogos();
        this.limpiarDatos();
    }
    public void onServicioSelected(){
        this.cargarCatalogos();
        if (servicioSelected!=null){
            this.tipoServicioSelected=servicioSelected.getTipoServicio().getCodigo();
            this.nombre = servicioSelected.getNombre();
            this.precio=servicioSelected.getPrecio();
            this.descripcion=servicioSelected.getDescripcion();
        }
    }

    private void cargarServicios(){
        listaServicios= this.servicioService.listarServicios();
    }
    private void cargarCatalogos(){
        listaTipoServicio= detalleCatalogoService.listarPorCodigo(TipoCatalogos.TIPO_SERVICIO.getCodigo());
    }
    public void gestionServicios(){
        try {

            Servicio servicio= new Servicio();
            if (servicioSelected!=null){
                servicio.setId(servicioSelected.getId());
                servicio.setTipoServicio(this.detalleCatalogoService.encontrarPorCodigo(this.tipoServicioSelected));
                servicio.setNombre(this.nombre);
                servicio.setDescripcion(this.descripcion);
                servicio.setPrecio(this.precio);

                this.servicioService.modificarServicio(servicio);

                mostrarMensajeSuccess("Servicio modficado correctamente");

            }else{
                servicio.setTipoServicio(this.detalleCatalogoService.encontrarPorCodigo(this.tipoServicioSelected));
                servicio.setNombre(this.nombre);
                servicio.setDescripcion(this.descripcion);
                servicio.setPrecio(this.precio);

                this.servicioService.agregarServicio(servicio);

                mostrarMensajeSuccess("Servicio agregado correctamente");
            }
            this.cargarServicios();

        }catch (Exception e){
            mostrarMensajeError(this.getClass().getSimpleName(),"gestionServicios","error al guardar registro",e);
        }
    }

    public List<Servicio> getListaServicios() {
        return listaServicios;
    }

    public void setListaServicios(List<Servicio> listaServicios) {
        this.listaServicios = listaServicios;
    }

    public Servicio getServicioSelected() {
        return servicioSelected;
    }

    public void setServicioSelected(Servicio servicioSelected) {
        this.servicioSelected = servicioSelected;
    }

    public List<DetalleCatalogo> getListaTipoServicio() {
        return listaTipoServicio;
    }

    public void setListaTipoServicio(List<DetalleCatalogo> listaTipoServicio) {
        this.listaTipoServicio = listaTipoServicio;
    }

    public String getTipoServicioSelected() {
        return tipoServicioSelected;
    }

    public void setTipoServicioSelected(String tipoServicioSelected) {
        this.tipoServicioSelected = tipoServicioSelected;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }
}
