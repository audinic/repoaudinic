package ni.com.audinic.view.backbean.gestionSolicitudes;

import com.googlecode.genericdao.search.SearchResult;
import ni.com.audinic.businesslogic.service.*;
import ni.com.audinic.model.catalogos.Cliente;
import ni.com.audinic.model.producto.DetalleSolicitud;
import ni.com.audinic.model.producto.Solicitud;
import ni.com.audinic.view.utils.JsfUtils;
import org.apache.commons.lang3.StringUtils;
import org.primefaces.model.LazyDataModel;
import org.primefaces.model.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;

import javax.annotation.PostConstruct;
import javax.inject.Named;
import java.util.List;
import java.util.Map;

/**
 * Creado por fmgarcia el 05 / 07 / 2017.
 */
@Named
@Scope("view")
public class recepcionSolicitudBean extends JsfUtils {


    private LazyDataModel<Solicitud> listaSolicitudes;
    private List<DetalleSolicitud> listaDetalleSolicitudes;
    private Solicitud solicitudSelected;

    //SERVICIOS
    private final DetalleCatalogoService detalleCatalogoService;
    private final ServicioService servicioService;
    private final EquipoService equipoService;
    private final ClienteService clienteService;
    private final SolicitudService solicitudService;
    private final DetalleSolicitudService detalleSolicitudService;

    @Autowired
    public recepcionSolicitudBean(final DetalleCatalogoService detalleCatalogoService,
                                  final ServicioService servicioService,
                                  final EquipoService equipoService,
                                  final ClienteService clienteService,
                                  final SolicitudService solicitudService,
                                  final DetalleSolicitudService detalleSolicitudService) {

        this.clienteService=clienteService;
        this.detalleCatalogoService=detalleCatalogoService;
        this.servicioService=servicioService;
        this.equipoService=equipoService;
        this.solicitudService=solicitudService;
        this.detalleSolicitudService=detalleSolicitudService;
    }

    @PostConstruct
    public void init(){
        iniciarLazyDataModelSolicitudes();
    }

    public void agregarSolicitud(){

    }

    private void iniciarLazyDataModelSolicitudes() {
        try {
            this.listaSolicitudes = new LazyDataModel<Solicitud>() {

                private static final long serialVersionUID = 1L;

                @Override
                public Solicitud getRowData(String rowKey) {
                    for (Solicitud evaluacionAdmitida : listaSolicitudes) {
                        if (Long.toString(evaluacionAdmitida.getId()).equalsIgnoreCase(rowKey)) {
                            return evaluacionAdmitida;
                        }
                    }

                    return null;
                }

                @Override
                public Object getRowKey(Solicitud evaluacionAdmitida) {
                    return Long.toString(evaluacionAdmitida.getId());
                }

                @Override
                public List<Solicitud> load(int first, int pageSize,
                                                String sortField, SortOrder sortOrder,
                                                Map<String, Object> filters) {



//                    if (filters.size() > 0) {
//                        // Filtro por Nombre de Tipo de Producto
//                        setFiltroTipoProducto((String) filters.get("guiaEvaluacion.tipoProducto.nombre"));
//                    }
                    @SuppressWarnings("rawtypes")
                    SearchResult searchResult = solicitudService.listarSolicitudes(
                            first
                            , pageSize);

                    this.setRowCount(searchResult.getTotalCount());
                    return searchResult.getResult();
                }
            };
        } catch (Exception e) {
            mostrarMensajeError(this.getClass().getSimpleName(), "iniciarLazyDataModelSolicitud()", "Error al cargar lista Solicitudes", e);
        }
    }


    public LazyDataModel<Solicitud> getListaSolicitudes() {
        return listaSolicitudes;
    }

    public void setListaSolicitudes(LazyDataModel<Solicitud> listaSolicitudes) {
        this.listaSolicitudes = listaSolicitudes;
    }

    public List<DetalleSolicitud> getListaDetalleSolicitudes() {
        return listaDetalleSolicitudes;
    }

    public void setListaDetalleSolicitudes(List<DetalleSolicitud> listaDetalleSolicitudes) {
        this.listaDetalleSolicitudes = listaDetalleSolicitudes;
    }

    public Solicitud getSolicitudSelected() {
        return solicitudSelected;
    }

    public void setSolicitudSelected(Solicitud solicitudSelected) {
        this.solicitudSelected = solicitudSelected;
    }

}
