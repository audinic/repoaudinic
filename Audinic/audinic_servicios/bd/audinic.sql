﻿
--Creamos los esquemas
CREATE SCHEMA catalogo_audinic;
CREATE SCHEMA producto_audinic;
CREATE SCHEMA seguridad;

CREATE TABLE seguridad.menu
(
    id serial,
    nombre character varying(100) NOT NULL, -- nombre del menu
    url character varying(1000) , -- ruta del archivo xhtml
    padre_menu integer, -- cual es el padre del menu
    orden integer NOT NULL, -- orden a mostrar los menu
    pasivo boolean DEFAULT false,
    CONSTRAINT menus_pkey PRIMARY KEY (id),
    CONSTRAINT menu_menu_id_fkey FOREIGN KEY (padre_menu)
    REFERENCES seguridad.menu (id) MATCH SIMPLE
    ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TABLE catalogo_audinic.catalogo (
    id serial primary key NOT NULL,
    codigo character varying(32) NOT NULL,
    nombre character varying(64) NOT NULL,
    descripcion character varying(512),
    activo boolean NOT NULL,
    CONSTRAINT catalogos_codigo_uq UNIQUE (codigo)
);

CREATE TABLE catalogo_audinic.catalogo_detalle (
    id serial primary key NOT NULL,
    ref_catalogo character varying(32) NOT NULL,
    codigo character varying(32) NOT NULL,
    valor character varying(512),
    descripcion character varying(512),
    activo boolean NOT NULL,
    CONSTRAINT catalogo_detalle_codigo_uq UNIQUE (codigo),
    CONSTRAINT fk_catalogo_detalle_catalogo FOREIGN KEY (ref_catalogo)
      REFERENCES catalogo_audinic.catalogo (codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE catalogo_audinic.equipos (
    id serial primary key NOT NULL,
    tipo_protesis integer NOT NULL, --tipo de protesis P(Programable) y M(Manual)
    modelo integer NOT NULL, --Valor catalogo (CIC,CANAL,ITE,BTE)
    descripcion character varying(500) NOT NULL,    
    numero_baterias integer NOT NULL,
    duracion_bateria integer,
    tipo_bateria integer,
    precio numeric(10,2) NOT NULL
);

CREATE TABLE catalogo_audinic.servicios (
    id serial primary key NOT NULL,
    tipo_servicio integer NOT NULL, --Valor catalogo que puede ser (Examen, protesis, mantenimiento, reparacion.)
    id_equipo integer, --equipo
    nombre character varying(100) NOT NULL,
    descripcion character varying(200),
    precio numeric(10,2) NOT NULL
);

CREATE TABLE catalogo_audinic.departamento (
    id integer NOT NULL,
    codigo character varying(32) NOT NULL,
    nombre character varying(150) NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    CONSTRAINT uq_departamento_codigo UNIQUE (codigo)
);

CREATE TABLE catalogo_audinic.municipio (
    id integer NOT NULL,
    codigo character varying(32) NOT NULL,
    ref_departamento character varying(32) NOT NULL,
    nombre character varying(150) NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    CONSTRAINT fk_municipio_departamento FOREIGN KEY (ref_departamento)
      REFERENCES catalogo_audinic.departamento (codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uq_municipio_codigo UNIQUE (codigo)
);

CREATE TABLE catalogo_audinic.cliente (
    id serial primary key NOT NULL,
    numero_identificacion character varying(20) NOT NULL,
    categoria integer, -- Asegurado o Pensionado
    nss character varying(20),
    primer_nombre character varying(30) NOT NULL,
    segundo_nombre character varying(30),
    primer_apellido character varying(30) NOT NULL,
    segundo_apellido character varying(30),
    fecha_nacimiento date,
    sexo integer NOT NULL, -- valor catalogo
    telefono character varying(50),
    correo character varying(50),
    municipio character varying(32) NOT NULL,
    direccion_domiciliar character varying(1000),
    activo boolean DEFAULT true NOT NULL,
    CONSTRAINT fk_cliente_municipio FOREIGN KEY (municipio)
      REFERENCES catalogo_audinic.municipio (codigo) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_cliente_sexo FOREIGN KEY (sexo)
      REFERENCES catalogo_audinic.catalogo_detalle (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT numero_identificacion_uq UNIQUE (numero_identificacion)
);

CREATE TABLE producto_audinic.solicitud (
    id serial primary key NOT NULL,
    origen integer NOT NULL, --INSS, Alcaldia, Privado
    numero_solicitud character varying(50),
    cliente integer NOT NULL,    
    situacion character varying(2) NOT NULL, --Pendiente ( P),Tramitada (T), ENTREGADA(E), Anulada (A)
    solicitud_padre INTEGER NOT NULL,
    fecha_creado date NOT NULL,
    fecha_generacion date,
    fecha_recepcion date,
    fecha_verificacion date,
    fecha_entrega date,
    cobrada boolean,
    CONSTRAINT fk_solicitud_padre FOREIGN KEY (solicitud_padre)
      REFERENCES producto_audinic.solicitud (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT fk_solicitud_cliente FOREIGN KEY (cliente)
      REFERENCES catalogo_audinic.cliente (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

CREATE TABLE producto_audinic.detalle_solicitud(
    id serial primary key NOT NULL,
    solicitud integer NOT NULL,
    id_servicio integer,
    id_equipo INTEGER,
    cantidad integer ,
    precio numeric(10,2) NOT NULL ,
    CONSTRAINT fk_servicio_solicitud_detalle FOREIGN KEY (id_servicio)
    REFERENCES catalogo_audinic.servicios (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
    CONSTRAINT fk_equio_solicitud_detalle FOREIGN KEY (id_equipo)
    REFERENCES catalogo_audinic.equipos (id) MATCH SIMPLE
    ON UPDATE NO ACTION ON DELETE NO ACTION,
     CONSTRAINT fk_solicitud_detalle FOREIGN KEY (solicitud)
      REFERENCES producto_audinic.solicitud (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
);

