INSERT INTO catalogo_audinic.catalogo VALUES (8, 'SEXO', 'SEXO O GENERO DE LA PERSONA', 'SEXO O GENERO DE LA PERSONA', true);
INSERT INTO catalogo_audinic.catalogo VALUES (11, 'TSERV', 'TIPO DE SERVICIO', 'TIPO DE SERVCIO BRINDADOS', true);
INSERT INTO catalogo_audinic.catalogo VALUES (12, 'TPPRO', 'TIPO DE PROTESIS', 'TIPO DE PROTESIS AUDITIVA', true);
INSERT INTO catalogo_audinic.catalogo VALUES (13, 'MDLEQ', 'MODELO DE EQUIPOS O PROTESIS', 'VALOR CATALOGO DE LOS MODELOS DE PROTESIS QUE SE OFRECEN', true);
INSERT INTO catalogo_audinic.catalogo VALUES (16, 'TPBAT', 'TIPO DE BATERIAS', 'TIPO DE BATERIAS USADAS POR LAS PROTESIS O EQUIPOS', true);
INSERT INTO catalogo_audinic.catalogo VALUES (17, 'CATCLIENTE', 'CATEGORIA DEL CLIENTE', 'CATOLOGO DE LA CATEGORIA DEL CLIENTE', true);
INSERT INTO catalogo_audinic.catalogo VALUES (18, 'ORIGENSOL', 'ORIGEN DE LA SOLICITUD', 'VALOR CATALOGO DE LAS EMPRESAS QUE GENERAN LA SOLICITUD', true);


INSERT INTO catalogo_audinic.catalogo_detalle VALUES (1, 'SEXO', 'M', 'MASCULINO', 'MASCULINO', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (2, 'SEXO', 'F', 'FEMENINO', 'FEMENINO', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (9, 'TSERV', 'TSERV_1', 'EXAMEN', 'EXAMEN', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (10, 'TSERV', 'TSERV_2', 'PROTESIS', 'PROTESIS', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (13, 'TPPRO', 'TPPRO_1', 'PROGRAMABLE', 'SON LAS PROTESIS QUE SE CONFIGURAN USANDO ALGUN SOFTWARE', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (14, 'TPPRO', 'TPPRO_2', 'MANUALES', 'SON LAS PROTESIS QUE SE CONFIGURAN MANUALMENTE', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (36, 'CATCLIENTE', 'CATCLIENTE_1', 'ASEGURADO', 'ASEGURADO', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (37, 'CATCLIENTE', 'CATCLIENTE_2', 'PENSIONADO', 'PENSIONADO', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (38, 'CATCLIENTE', 'CATCLIENTE_3', 'PRIVADO', 'PRIVADO', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (39, 'ORIGENSOL', 'ORIGENSOL_1', 'INSS', 'INSTITUTO DE SEGURIDAD SOCIAL', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (40, 'ORIGENSOL', 'ORIGENSOL_2', 'ALCALDIA', 'ALCALDIA VALOR GENERICO, PUEDE SER ALCALDIA DEPENDIENDO DEL DEPARTAMENTO DONDE ESTE', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (41, 'ORIGENSOL', 'ORIGENSOL_3', 'PRIVADO', 'SE USA PARA CLIENTES QUE LLEGUEN POR SU PROPIA CUENTA', true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (42, 'MDLEQ', 'MDLEQ_1', 'CIC', NULL, true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (43, 'MDLEQ', 'MDLEQ_2', 'CANAL', NULL, true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (44, 'MDLEQ', 'MDLEQ_3', 'ITE', NULL, true);
INSERT INTO catalogo_audinic.catalogo_detalle VALUES (45, 'MDLEQ', 'MDLEQ_4', 'BTE', NULL, true);


SELECT pg_catalog.setval('catalogo_audinic.catalogo_detalle_id_seq', 45, true);
SELECT pg_catalog.setval('catalogo_audinic.catalogo_id_seq', 18, true);




