package ni.com.audinic.businesslogic.service;

import java.util.List;
import ni.com.audinic.model.catalogos.Cliente;
import ni.com.audinic.repository.dao.DAOException;

public interface ClienteService {

    void agregarCliente(Cliente cliente)throws DAOException;

    void modificarCliente(Cliente cliente)throws DAOException;

    boolean eliminarCliente(Cliente cliente);

    List<Cliente> listarClientes();

    Cliente obtenerClientePorId(Integer id);
}
