package ni.com.audinic.businesslogic.service;

import ni.com.audinic.model.seguridad.Menu;
import ni.com.audinic.repository.dao.DAOException;

import java.util.List;

/**
 * Creado por fmgarcia el 15 / 06 / 2017.
 */
public interface MenuService {

    public void agregar(Menu menu)throws DAOException;

    public List<Menu> listarMenus();

     Menu encontrarPorId(Integer idMenu);
}
