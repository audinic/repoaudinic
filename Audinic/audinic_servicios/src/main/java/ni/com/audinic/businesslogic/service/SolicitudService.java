package ni.com.audinic.businesslogic.service;

import java.util.List;

import com.googlecode.genericdao.search.SearchResult;
import ni.com.audinic.model.producto.Solicitud;
import ni.com.audinic.repository.dao.DAOException;



public interface SolicitudService {

    void agregarSolicitud(Solicitud solicitud)throws DAOException;

    void ModificarSolicitud(Solicitud solicitud)throws DAOException;

    boolean eliminarSolicitud(Solicitud solicitud);

    SearchResult<Solicitud> listarSolicitudes(final int paginaActual
            , final int totalPorPaginar);

    Solicitud obtenerSolicitudPorId(Integer id);
}
