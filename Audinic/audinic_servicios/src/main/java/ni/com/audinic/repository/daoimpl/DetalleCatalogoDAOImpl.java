package ni.com.audinic.repository.daoimpl;

import ni.com.audinic.model.catalogos.Catalogo;
import ni.com.audinic.model.catalogos.DetalleCatalogo;
import ni.com.audinic.repository.dao.DetalleCatalogoDAO;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Creado por fmgarcia el 19 / 06 / 2017.
 */
@Repository
public class DetalleCatalogoDAOImpl extends GenericDAOImpl<DetalleCatalogo,Integer> implements DetalleCatalogoDAO{

    @Override
    @SuppressWarnings("unchecked")
    public List<DetalleCatalogo> listarTodo() {
        Query query = sessionFactory.getCurrentSession().createQuery("select dc from DetalleCatalogo dc where dc.activo=true");
        return (List<DetalleCatalogo>) query.list();
    }
}
