package ni.com.audinic.repository.daoimpl;

import org.springframework.stereotype.Repository;
import ni.com.audinic.model.catalogos.Cliente;
import ni.com.audinic.repository.dao.ClienteDAO;

@Repository
public class ClienteDAOImpl extends GenericDAOImpl<Cliente, Integer> implements ClienteDAO {

}
