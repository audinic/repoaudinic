package ni.com.audinic.rest.client.jasperserver;

import javax.servlet.http.HttpServletResponse;
import java.util.Map;

public interface JasperRestService {

    public byte[] getReport(String resourceUri, String type, Map<String, String> resourceParameters) throws Exception;

    public void getReport(String host, Integer port, String user, String password, String resourceUri, String type,
            Map<String, String> resourceParameters, String fileName) throws Exception;

    public void getReport(String resourceUri, String type, Map<String, String> resourceParameters, String fileName)
            throws Exception;

    void getReport(String resourceUri, String type, Map<String, String> resourceParameters, String fileName,
            HttpServletResponse response) throws Exception;
}
