package ni.com.audinic.repository.dao;

import ni.com.audinic.model.catalogos.Cliente;

public interface ClienteDAO extends GenericDAO<Cliente, Integer> {

}
