package ni.com.audinic.model.catalogos;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Creado por fmgarcia el 07 / 06 / 2017.
 */
@Entity
@Table(name = "catalogo_detalle", schema = "catalogo_audinic", uniqueConstraints = @UniqueConstraint(columnNames = "codigo"))
public class DetalleCatalogo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Catalogo catalogo;
    private String codigo;
    private String valor;
    private String descripcion;
    private boolean activo;


    @Id
    @SequenceGenerator(name = "pk_detalle_catalogo", sequenceName = "catalogo_audinic.catalogo_detalle_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_detalle_catalogo")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Catalogo.class)
    @JoinColumn(name = "ref_catalogo",referencedColumnName = "codigo")
    public Catalogo getCatalogo() {
        return catalogo;
    }

    public void setCatalogo(Catalogo catalogo) {
        this.catalogo = catalogo;
    }

    @Column(name = "codigo", nullable = false, length = 32)
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


    @Column(name = "valor", nullable = true, length = 512)
    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }


    @Column(name = "descripcion", nullable = true, length = 512)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "activo", nullable = false)
    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }
}
