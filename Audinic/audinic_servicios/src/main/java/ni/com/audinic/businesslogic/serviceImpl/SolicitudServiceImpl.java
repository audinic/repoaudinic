package ni.com.audinic.businesslogic.serviceImpl;

import java.util.List;

import com.googlecode.genericdao.search.SearchResult;
import com.googlecode.genericdao.search.Sort;
import ni.com.audinic.repository.dao.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import ni.com.audinic.businesslogic.service.SolicitudService;
import ni.com.audinic.model.producto.Solicitud;
import ni.com.audinic.repository.dao.SolicitudDAO;

@Service
@Transactional
public class SolicitudServiceImpl implements SolicitudService {

    @Autowired
    SolicitudDAO solicitudDAO;
    
    @Override
    public void agregarSolicitud(Solicitud solicitud) throws DAOException {
        solicitudDAO.saveUpper(solicitud);
    }

    @Override
    public void ModificarSolicitud(Solicitud solicitud) throws DAOException{
        solicitudDAO.updateUpper(solicitud);
    }

    @Override
    public boolean eliminarSolicitud(Solicitud solicitud) {
        return solicitudDAO.remove(solicitud);
    }

    @Override
    public SearchResult<Solicitud> listarSolicitudes(final int paginaActual
            , final int totalPorPaginar) {

        int pActual=paginaActual;
        int totalPagina=totalPorPaginar;
        SearchResult<Solicitud> searchResult = new SearchResult<>();

        Search search = new Search();
        search.addSortAsc("numeroSolicitud");

        int numRegistros = solicitudDAO.count(search);
        searchResult.setTotalCount(numRegistros);

        if (numRegistros > 0) {
            if (numRegistros <= pActual) {
                pActual -= numRegistros;
            }

            pActual = numRegistros <= pActual ? 0 : pActual;
            search.setFirstResult(pActual);
            search.setMaxResults(totalPagina);
            search.addFetches("solicitud_padre","cliente","origen");

            searchResult.setResult( (List<Solicitud>) solicitudDAO.search(search));

        }

        return searchResult;
    }

    @Override
    public Solicitud obtenerSolicitudPorId(Integer id) {
        Search search = new Search();
        search.addFilterEqual("id", id);
        return solicitudDAO.searchUnique(search);
    }

}
