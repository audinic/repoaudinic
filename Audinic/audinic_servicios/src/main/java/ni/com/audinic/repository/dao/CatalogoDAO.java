package ni.com.audinic.repository.dao;

import ni.com.audinic.model.catalogos.Catalogo;

import java.util.List;

/**
 * Creado por fmgarcia el 19 / 06 / 2017.
 */
public interface CatalogoDAO extends GenericDAO<Catalogo,Integer> {

    List<Catalogo> listarTodo();

}
