package ni.com.audinic.businesslogic.serviceImpl;

import java.util.List;

import ni.com.audinic.repository.dao.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import ni.com.audinic.businesslogic.service.ServicioService;
import ni.com.audinic.model.catalogos.Servicio;
import ni.com.audinic.repository.dao.ServicioDAO;

@Service
@Transactional
public class ServicioServiceImpl implements ServicioService {

    @Autowired
    ServicioDAO servicioDAO;
    
    @Override
    public void agregarServicio(Servicio servicio) throws DAOException {
        servicioDAO.saveUpper(servicio);
    }

    @Override
    public void modificarServicio(Servicio servicio) throws DAOException{
        servicioDAO.updateUpper(servicio);
    }

    @Override
    public boolean eliminarServicio(Servicio servicio) {
        return servicioDAO.remove(servicio);
    }

    @Override
    public List<Servicio> listarServicios() {
        Search search = new Search();
        search.addFetch("tipoServicio");
        search.addSortAsc("nombre");
        return servicioDAO.search(search);
    }

    @Override
    public Servicio obtenerServicioPorId(Integer id) {
        Search search = new Search();
        search.addFilterEqual("id", id);
        return servicioDAO.searchUnique(search);
    }

}
