package ni.com.audinic.repository.dao;

import ni.com.audinic.model.seguridad.Menu;

/**
 * Creado por fmgarcia el 15 / 06 / 2017.
 */
public interface MenuDAO extends GenericDAO<Menu,Integer> {

}
