package ni.com.audinic.repository.daoimpl;

import org.springframework.stereotype.Repository;
import ni.com.audinic.model.producto.DetalleSolicitud;
import ni.com.audinic.repository.dao.DetalleSolicitudDAO;

@Repository
public class DetalleSolicitudDAOImpl extends GenericDAOImpl<DetalleSolicitud, Integer> implements DetalleSolicitudDAO {

}
