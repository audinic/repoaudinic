package ni.com.audinic.repository.dao;

/**
 * Creado por fmgarcia el 27 / 06 / 2017.
 */
public class DAOException extends Exception {

    public DAOException(String msg) {
        super(msg);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }

    public DAOException(String msg, Throwable cause) {
        super(msg, cause);
    }
}
