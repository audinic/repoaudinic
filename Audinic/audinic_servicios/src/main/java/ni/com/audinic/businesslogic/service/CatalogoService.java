package ni.com.audinic.businesslogic.service;

import ni.com.audinic.model.catalogos.Catalogo;
import ni.com.audinic.repository.dao.DAOException;

import java.util.List;

/**
 * Creado por fmgarcia el 19 / 06 / 2017.
 */
public interface CatalogoService {

    void agregar(Catalogo catalogo)throws DAOException;

    void modificar(Catalogo catalogo)throws DAOException;

    List<Catalogo> listarCatalogos(String filtro);

}
