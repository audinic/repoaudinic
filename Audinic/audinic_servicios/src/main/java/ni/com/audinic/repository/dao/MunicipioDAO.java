package ni.com.audinic.repository.dao;

import ni.com.audinic.model.catalogos.Municipio;

public interface MunicipioDAO extends GenericDAO<Municipio, Integer> {

}
