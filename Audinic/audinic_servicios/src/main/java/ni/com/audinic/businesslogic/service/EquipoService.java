package ni.com.audinic.businesslogic.service;

import java.util.List;

import ni.com.audinic.model.catalogos.Equipos;
import ni.com.audinic.repository.dao.DAOException;

public interface EquipoService {
    
    void agregarEquipo(Equipos equipo)throws DAOException;

    void modificarEquipo(Equipos equipo)throws DAOException;

    boolean eliminarEquipo(Equipos equipo);

    List<Equipos> listarEquipos();

    Equipos obtenerEquipoPorId(Integer id);
}
