package ni.com.audinic.businesslogic.service;

import java.util.List;

import ni.com.audinic.model.producto.DetalleSolicitud;
import ni.com.audinic.repository.dao.DAOException;

public interface DetalleSolicitudService {

    void agregarDetalleSolicitud(DetalleSolicitud detalleSolicitud)throws DAOException;

    void ModificarDetalleSolicitud(DetalleSolicitud detalleSolicitud)throws DAOException;

    boolean eliminarDetalleSolicitud(DetalleSolicitud detalleSolicitud);

    List<DetalleSolicitud> listarDetalleSolicitudesPorIdSolicitud(Integer id);

    DetalleSolicitud obtenerDetalleSolicitudPorId(Integer id);
}
