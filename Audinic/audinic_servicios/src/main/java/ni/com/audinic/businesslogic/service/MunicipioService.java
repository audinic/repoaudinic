package ni.com.audinic.businesslogic.service;

import java.util.List;

import ni.com.audinic.model.catalogos.Municipio;
import ni.com.audinic.repository.dao.DAOException;

public interface MunicipioService {

    void agregarMunicipio(Municipio municipio)throws DAOException;

    void ModificarMunicipio(Municipio municipio)throws DAOException;

    boolean eliminarMunicipio(Municipio municipio);

    List<Municipio> listarMunicipiosPorDepartamento(String departamento);

    Municipio obtenerMunicipioPorCodigo(String codigo);
}
