package ni.com.audinic.repository.dao;

import ni.com.audinic.model.catalogos.Servicio;

public interface ServicioDAO extends GenericDAO<Servicio, Integer>{

}
