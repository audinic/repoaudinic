package ni.com.audinic.businesslogic.serviceImpl;

import java.util.List;

import ni.com.audinic.repository.dao.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import ni.com.audinic.businesslogic.service.MunicipioService;
import ni.com.audinic.model.catalogos.Municipio;
import ni.com.audinic.repository.dao.MunicipioDAO;

@Service
@Transactional
public class MunicipioServiceImpl implements MunicipioService {

    @Autowired
    MunicipioDAO municipioDAO;

    @Override
    public void agregarMunicipio(Municipio municipio) throws DAOException {
        municipioDAO.saveUpper(municipio);
    }

    @Override
    public void ModificarMunicipio(Municipio municipio) throws DAOException {
        municipioDAO.updateUpper(municipio);
    }

    @Override
    public boolean eliminarMunicipio(Municipio municipio) {
        return municipioDAO.remove(municipio);
    }

    @Override
    public List<Municipio> listarMunicipiosPorDepartamento(String departamento) {
        Search search = new Search();
        search.addFilterEqual("departamento.codigo", departamento);
        search.addSortAsc("nombre");
        
        return municipioDAO.search(search);
    }

    @Override
    public Municipio obtenerMunicipioPorCodigo(String codigo) {
        Search search = new Search();
        search.addFilterEqual("codigo", codigo);
        return municipioDAO.searchUnique(search);
    }

}
