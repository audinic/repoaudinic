package ni.com.audinic.repository.daoimpl;

import org.springframework.stereotype.Repository;
import ni.com.audinic.model.catalogos.Equipos;
import ni.com.audinic.repository.dao.EquipoDAO;

@Repository
public class EquipoDAOImpl extends GenericDAOImpl<Equipos, Integer> implements EquipoDAO {

}
