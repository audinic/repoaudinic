package ni.com.audinic.model.catalogos;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Creado por fmgarcia el 07 / 06 / 2017.
 */
@Entity
@Table(name = "municipio", schema = "catalogo_audinic", uniqueConstraints = @UniqueConstraint(columnNames = "codigo"))
public class Municipio implements Serializable{
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Departamento departamento;
    private String codigo;
    private String nombre;
    private Boolean activo;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ref_departamento", referencedColumnName = "codigo")
    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    @Column(name = "codigo", nullable = false, length = 32)
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


    @Column(name = "nombre", nullable = false, length = 150)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Column(name = "activo", nullable = false)
    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Municipio municipio = (Municipio) o;

        if (id != null ? !id.equals(municipio.id) : municipio.id != null) return false;
        if (codigo != null ? !codigo.equals(municipio.codigo) : municipio.codigo != null) return false;
        if (nombre != null ? !nombre.equals(municipio.nombre) : municipio.nombre != null) return false;
        if (activo != null ? !activo.equals(municipio.activo) : municipio.activo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (codigo != null ? codigo.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (activo != null ? activo.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
}
