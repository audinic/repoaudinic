package ni.com.audinic.repository.daoimpl;

import org.springframework.stereotype.Repository;
import ni.com.audinic.model.catalogos.Municipio;
import ni.com.audinic.repository.dao.MunicipioDAO;

@Repository
public class MunicipioDAOImpl extends GenericDAOImpl<Municipio, Integer> implements MunicipioDAO {

}
