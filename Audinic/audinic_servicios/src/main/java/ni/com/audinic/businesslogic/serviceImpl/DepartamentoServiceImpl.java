package ni.com.audinic.businesslogic.serviceImpl;

import java.util.List;

import ni.com.audinic.repository.dao.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import ni.com.audinic.businesslogic.service.DepartamentoService;
import ni.com.audinic.model.catalogos.Departamento;
import ni.com.audinic.repository.dao.DepartamentoDAO;

@Service
@Transactional
public class DepartamentoServiceImpl implements DepartamentoService {

    @Autowired
    DepartamentoDAO departamentoDAO;
    
    @Override
    public void agregarDepartamento(Departamento departamento) throws DAOException {
        departamentoDAO.saveUpper(departamento);
    }

    @Override
    public void ModificarDepartamento(Departamento departamento) throws DAOException{
        departamentoDAO.updateUpper(departamento);
    }

    @Override
    public boolean eliminarDepartamento(Departamento departamento) {
        return departamentoDAO.remove(departamento);
    }

    @Override
    public List<Departamento> listarDepartamentos() {
        Search search = new Search();
        search.addSortAsc("nombre");
        return departamentoDAO.search(search);
    }

    @Override
    public Departamento obtenerDepartamentoPorId(Integer id) {
        Search search = new Search();
        search.addFilterEqual("id", id);
        return departamentoDAO.searchUnique(search);
    }

}
