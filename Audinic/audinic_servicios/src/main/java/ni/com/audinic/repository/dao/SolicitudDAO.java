package ni.com.audinic.repository.dao;

import ni.com.audinic.model.producto.Solicitud;

public interface SolicitudDAO extends GenericDAO<Solicitud, Integer> {

}
