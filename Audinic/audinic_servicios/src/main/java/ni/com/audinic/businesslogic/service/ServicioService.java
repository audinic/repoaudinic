package ni.com.audinic.businesslogic.service;

import java.util.List;

import ni.com.audinic.model.catalogos.Servicio;
import ni.com.audinic.repository.dao.DAOException;

public interface ServicioService {

    void agregarServicio(Servicio servicio)throws DAOException;

    void modificarServicio(Servicio servicio)throws DAOException;

    boolean eliminarServicio(Servicio servicio);

    List<Servicio> listarServicios();

    Servicio obtenerServicioPorId(Integer id);
}
