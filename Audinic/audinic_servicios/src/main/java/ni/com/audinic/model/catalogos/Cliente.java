package ni.com.audinic.model.catalogos;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Creado por fmgarcia el 07 / 06 / 2017.
 */
@Entity
@Table(name = "cliente", schema = "catalogo_audinic")
public class Cliente  implements Serializable{
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String numeroIdentificacion;
    private DetalleCatalogo categoria;
    private String nss;
    private DetalleCatalogo sexo;
    private String primerNombre;
    private String segundoNombre;
    private String primerApellido;
    private String segundoApellido;
    private Date fechaNacimiento;
    private String telefono;
    private String correo;
    private Municipio municipio;
    private String direccionDomiciliar;
    private Boolean activo;

    @Id
    @SequenceGenerator(name = "pk_cliente", sequenceName = "catalogo_audinic.cliente_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_cliente")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "numero_identificacion", nullable = false, length = 20)
    public String getNumeroIdentificacion() {
        return numeroIdentificacion;
    }

    public void setNumeroIdentificacion(String numeroIdentificacion) {
        this.numeroIdentificacion = numeroIdentificacion;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "categoria", referencedColumnName = "id")
    public DetalleCatalogo getCategoria() {
        return categoria;
    }

    public void setCategoria(DetalleCatalogo categoria) {
        this.categoria = categoria;
    }

    @Column(name = "nss", nullable = true, length = 20)
    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    @Column(name = "primer_nombre", nullable = false, length = 30)
    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    @Column(name = "segundo_nombre", nullable = true, length = 30)
    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    @Column(name = "primer_apellido", nullable = false, length = 30)
    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    @Column(name = "segundo_apellido", nullable = true, length = 30)
    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    @Column(name = "fecha_nacimiento", nullable = true)
    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    @Column(name = "telefono", nullable = false, length = 50)
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Column(name = "correo", nullable = true, length = 50)
    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }
    
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "municipio", referencedColumnName = "codigo")
    public Municipio getMunicipio() {
        return municipio;
    }

    public void setMunicipio(Municipio municipio) {
        this.municipio = municipio;
    }

    @Column(name = "direccion_domiciliar", nullable = true, length = 1000)
    public String getDireccionDomiciliar() {
        return direccionDomiciliar;
    }

    public void setDireccionDomiciliar(String direccionDomiciliar) {
        this.direccionDomiciliar = direccionDomiciliar;
    }

    @ManyToOne(fetch = FetchType.LAZY, optional = false, targetEntity = DetalleCatalogo.class)
    @JoinColumn(name = "sexo", referencedColumnName = "id", nullable = false)
    public DetalleCatalogo getSexo() {
        return sexo;
    }

    public void setSexo(DetalleCatalogo sexo) {
        this.sexo = sexo;
    }

    @Column(name = "activo", nullable = false)
    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Cliente cliente = (Cliente) o;

        if (id != null ? !id.equals(cliente.id) : cliente.id != null)
            return false;
        if (numeroIdentificacion != null ? !numeroIdentificacion.equals(cliente.numeroIdentificacion)
                : cliente.numeroIdentificacion != null)
            return false;
        if (categoria != null ? !categoria.equals(cliente.categoria) : cliente.categoria != null)
            return false;
        if (nss != null ? !nss.equals(cliente.nss) : cliente.nss != null)
            return false;
        if (primerNombre != null ? !primerNombre.equals(cliente.primerNombre) : cliente.primerNombre != null)
            return false;
        if (segundoNombre != null ? !segundoNombre.equals(cliente.segundoNombre) : cliente.segundoNombre != null)
            return false;
        if (primerApellido != null ? !primerApellido.equals(cliente.primerApellido) : cliente.primerApellido != null)
            return false;
        if (segundoApellido != null ? !segundoApellido.equals(cliente.segundoApellido)
                : cliente.segundoApellido != null)
            return false;
        if (fechaNacimiento != null ? !fechaNacimiento.equals(cliente.fechaNacimiento)
                : cliente.fechaNacimiento != null)
            return false;
        if (telefono != null ? !telefono.equals(cliente.telefono) : cliente.telefono != null)
            return false;
        if (correo != null ? !correo.equals(cliente.correo) : cliente.correo != null)
            return false;
        if (direccionDomiciliar != null ? !direccionDomiciliar.equals(cliente.direccionDomiciliar)
                : cliente.direccionDomiciliar != null)
            return false;
        if (activo != null ? !activo.equals(cliente.activo) : cliente.activo != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (numeroIdentificacion != null ? numeroIdentificacion.hashCode() : 0);
        result = 31 * result + (categoria != null ? categoria.hashCode() : 0);
        result = 31 * result + (nss != null ? nss.hashCode() : 0);
        result = 31 * result + (primerNombre != null ? primerNombre.hashCode() : 0);
        result = 31 * result + (segundoNombre != null ? segundoNombre.hashCode() : 0);
        result = 31 * result + (primerApellido != null ? primerApellido.hashCode() : 0);
        result = 31 * result + (segundoApellido != null ? segundoApellido.hashCode() : 0);
        result = 31 * result + (fechaNacimiento != null ? fechaNacimiento.hashCode() : 0);
        result = 31 * result + (telefono != null ? telefono.hashCode() : 0);
        result = 31 * result + (correo != null ? correo.hashCode() : 0);
        result = 31 * result + (direccionDomiciliar != null ? direccionDomiciliar.hashCode() : 0);
        result = 31 * result + (activo != null ? activo.hashCode() : 0);
        return result;
    }
}
