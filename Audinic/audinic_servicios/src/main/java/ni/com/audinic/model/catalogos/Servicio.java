package ni.com.audinic.model.catalogos;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Creado por fmgarcia el 12 / 06 / 2017.
 */
@Entity
@Table(name = "servicios",schema = "catalogo_audinic")
public class Servicio implements Serializable{

    private Integer id;
    private DetalleCatalogo tipoServicio;
    private String nombre;
    private String descripcion;
    private BigDecimal precio;

    @Id
    @SequenceGenerator(name = "pk_servicio", sequenceName = "catalogo_audinic.servicios_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE ,generator = "pk_servicio")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_servicio",referencedColumnName = "id")
    public DetalleCatalogo getTipoServicio() {
        return tipoServicio;
    }

    public void setTipoServicio(DetalleCatalogo tipoServicio) {
        this.tipoServicio = tipoServicio;
    }


    @Column(name = "nombre", nullable = false, length = 100)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Column(name = "descripcion", nullable = true, length = 200)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    @Column(name = "precio", nullable = false, precision = 2)
    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Servicio servicio = (Servicio) o;

        if (id != null ? !id.equals(servicio.id) : servicio.id != null) return false;
        if (tipoServicio != null ? !tipoServicio.equals(servicio.tipoServicio) : servicio.tipoServicio != null)
            return false;
        if (nombre != null ? !nombre.equals(servicio.nombre) : servicio.nombre != null) return false;
        if (descripcion != null ? !descripcion.equals(servicio.descripcion) : servicio.descripcion != null)
            return false;
        if (precio != null ? !precio.equals(servicio.precio) : servicio.precio != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (tipoServicio != null ? tipoServicio.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (precio != null ? precio.hashCode() : 0);
        return result;
    }
}
