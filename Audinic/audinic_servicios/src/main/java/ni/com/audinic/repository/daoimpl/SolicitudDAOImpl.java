package ni.com.audinic.repository.daoimpl;

import org.springframework.stereotype.Repository;
import ni.com.audinic.model.producto.Solicitud;
import ni.com.audinic.repository.dao.SolicitudDAO;

@Repository
public class SolicitudDAOImpl extends GenericDAOImpl<Solicitud, Integer> implements SolicitudDAO {

}
