package ni.com.audinic.model.producto;

/**
 * Creado por fmgarcia el 29 / 06 / 2017.
 */
public enum SituacionSolicitud {

        P("P"),
        T("T"),
        E("E"),
        A("A");


        private final String codigo;

        private SituacionSolicitud(final String codigo)
        {
            this.codigo = codigo;
        }

        public String getCodigo(){
            return this.codigo;
        }

        public String getNombre()
        {
            String nombre = null;
            switch (this)
            {
                case P:
                    nombre = "Pendiente";
                    break;
                case T:
                    nombre = "En Tramite";
                    break;
                case E:
                    nombre = "Entregada";
                    break;
                case A:
                    nombre = "Anulada";
                    break;
            }
            return nombre;
        }

        public static SituacionSolicitud obtenerPorCodigo(final String codigo) throws IllegalArgumentException
        {
            for(SituacionSolicitud situacionTramiteRegistro : values())
            {
                if(situacionTramiteRegistro.getCodigo().equals(codigo))
                {
                    return situacionTramiteRegistro;
                }
            }
            throw new IllegalArgumentException("El código de la situación de la solicitud de registro de producto no es válido");
        }

        @Override
        public String toString()
        {
            return this.codigo;
        }



}
