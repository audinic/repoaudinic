package ni.com.audinic.model.catalogos;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Creado por fmgarcia el 07 / 06 / 2017.
 */

@Entity
@Table(name = "catalogo", schema = "catalogo_audinic", uniqueConstraints = @UniqueConstraint(columnNames = "codigo"))
public class Catalogo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer id;
    private String codigo;
    private String nombre;
    private String descripcion;
    private Boolean activo;

    @Id
    @SequenceGenerator(name = "pk_catalogo", sequenceName = "catalogo_audinic.catalogo_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_catalogo")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    @Column(name = "codigo", nullable = false, length = 32)
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }


    @Column(name = "nombre", nullable = false, length = 64)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }


    @Column(name = "descripcion", nullable = true, length = 512)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    @Column(name = "activo", nullable = false)
    public Boolean getActivo() {
        return activo;
    }

    public void setActivo(Boolean activo) {
        this.activo = activo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Catalogo catalogo = (Catalogo) o;

        if (id != null ? !id.equals(catalogo.id) : catalogo.id != null) return false;
        if (codigo != null ? !codigo.equals(catalogo.codigo) : catalogo.codigo != null) return false;
        if (nombre != null ? !nombre.equals(catalogo.nombre) : catalogo.nombre != null) return false;
        if (descripcion != null ? !descripcion.equals(catalogo.descripcion) : catalogo.descripcion != null)
            return false;
        if (activo != null ? !activo.equals(catalogo.activo) : catalogo.activo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (codigo != null ? codigo.hashCode() : 0);
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (activo != null ? activo.hashCode() : 0);
        return result;
    }
}
