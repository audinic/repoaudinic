package ni.com.audinic.businesslogic.service;

import java.util.List;

import ni.com.audinic.model.catalogos.Departamento;
import ni.com.audinic.repository.dao.DAOException;

public interface DepartamentoService {

    void agregarDepartamento(Departamento departamento)throws DAOException;

    void ModificarDepartamento(Departamento departamento)throws DAOException;

    boolean eliminarDepartamento(Departamento departamento);

    List<Departamento> listarDepartamentos();

    Departamento obtenerDepartamentoPorId(Integer id);
}
