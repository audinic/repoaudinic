package ni.com.audinic.model.catalogos;

/**
 * Creado por fmgarcia el 29 / 06 / 2017.
 */
public enum TipoCatalogos {

    SEXO("SEXO"),
    TIPO_SERVICIO("TSERV"),
    TIPO_PROTESIS("TPPRO"),
    MODELO_EQUIPO("MDLEQ"),
    TIPO_BATERIA("TPBAT"),
    CATEGORIA_CLIENTE("CATCLIENTE"),
    ORIGEN_SOLICITUD("ORIGENSOL");

    private final String codigo;

    TipoCatalogos(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }
}
