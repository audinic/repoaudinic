package ni.com.audinic.repository.dao;

import ni.com.audinic.model.producto.DetalleSolicitud;

public interface DetalleSolicitudDAO extends GenericDAO<DetalleSolicitud, Integer> {

}
