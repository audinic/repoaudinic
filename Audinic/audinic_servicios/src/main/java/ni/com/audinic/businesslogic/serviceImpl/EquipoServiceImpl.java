package ni.com.audinic.businesslogic.serviceImpl;

import java.util.List;

import ni.com.audinic.repository.dao.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import ni.com.audinic.businesslogic.service.EquipoService;
import ni.com.audinic.model.catalogos.Equipos;
import ni.com.audinic.repository.dao.EquipoDAO;

@Service
@Transactional
public class EquipoServiceImpl implements EquipoService {

    @Autowired
    EquipoDAO equipoDAO;
    
    @Override
    public void agregarEquipo(Equipos equipo) throws DAOException {
        equipoDAO.saveUpper(equipo);
    }

    @Override
    public void modificarEquipo(Equipos equipo) throws DAOException{
        equipoDAO.updateUpper(equipo);
    }

    @Override
    public boolean eliminarEquipo(Equipos equipo) {
        return equipoDAO.remove(equipo);
    }

    @Override
    public List<Equipos> listarEquipos() {
        Search search = new Search();
        search.addFetches("tipoProtesis","modelo","tipoBateria");
        search.addSortAsc("descripcion");
        return equipoDAO.search(search);
    }

    @Override
    public Equipos obtenerEquipoPorId(Integer id) {
        Search search = new Search();
        search.addFilterEqual("id", id);
        return equipoDAO.searchUnique(search);
    }

}
