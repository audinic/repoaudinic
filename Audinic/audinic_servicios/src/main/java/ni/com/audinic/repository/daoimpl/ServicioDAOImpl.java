package ni.com.audinic.repository.daoimpl;

import org.springframework.stereotype.Repository;
import ni.com.audinic.model.catalogos.Servicio;
import ni.com.audinic.repository.dao.ServicioDAO;

@Repository
public class ServicioDAOImpl extends GenericDAOImpl<Servicio, Integer> implements ServicioDAO {

}
