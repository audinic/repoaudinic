package ni.com.audinic.repository.dao;

import ni.com.audinic.model.catalogos.DetalleCatalogo;

import java.util.List;

/**
 * Creado por fmgarcia el 19 / 06 / 2017.
 */
public interface DetalleCatalogoDAO extends GenericDAO<DetalleCatalogo,Integer>  {

    List<DetalleCatalogo> listarTodo() ;
}
