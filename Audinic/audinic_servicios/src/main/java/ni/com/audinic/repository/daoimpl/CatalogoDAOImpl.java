package ni.com.audinic.repository.daoimpl;

import ni.com.audinic.model.catalogos.Catalogo;
import ni.com.audinic.repository.dao.CatalogoDAO;
import org.hibernate.Query;
import org.springframework.stereotype.Repository;

import javax.persistence.TypedQuery;
import java.util.List;

/**
 * Creado por fmgarcia el 19 / 06 / 2017.
 */
@Repository
public class CatalogoDAOImpl extends GenericDAOImpl<Catalogo,Integer> implements CatalogoDAO{


    @Override
    @SuppressWarnings("unchecked")
    public List<Catalogo> listarTodo() {
            Query query = sessionFactory.getCurrentSession().createQuery("select ct from Catalogo ct"
                    + " where ct.activo = true");
            return (List<Catalogo>) query.list();
    }
}
