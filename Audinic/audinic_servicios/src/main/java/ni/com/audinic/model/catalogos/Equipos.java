package ni.com.audinic.model.catalogos;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Creado por fmgarcia el 12 / 06 / 2017.
 */
@Entity
@Table(name = "equipos", schema = "catalogo_audinic")
public class Equipos {
    private Integer id;
    private DetalleCatalogo tipoProtesis;
    private DetalleCatalogo modelo;
    private String descripcion;
    private Integer numeroBaterias;
    private Integer duracionBateria;
    private DetalleCatalogo tipoBateria;
    private BigDecimal precio;

    @Id
    @SequenceGenerator(name = "pk_equipos", sequenceName = "catalogo_audinic.equipos_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_equipos")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_protesis", referencedColumnName = "id")
    public DetalleCatalogo getTipoProtesis() {
        return tipoProtesis;
    }

    public void setTipoProtesis(DetalleCatalogo tipoProtesis) {
        this.tipoProtesis = tipoProtesis;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "modelo", referencedColumnName = "id")
    public DetalleCatalogo getModelo() {
        return modelo;
    }

    public void setModelo(DetalleCatalogo modelo) {
        this.modelo = modelo;
    }

    @Column(name = "descripcion", nullable = false, length = 500)
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Column(name = "numero_baterias", nullable = false)
    public Integer getNumeroBaterias() {
        return numeroBaterias;
    }

    public void setNumeroBaterias(Integer numeroBaterias) {
        this.numeroBaterias = numeroBaterias;
    }

    @Column(name = "duracion_bateria", nullable = true)
    public Integer getDuracionBateria() {
        return duracionBateria;
    }

    public void setDuracionBateria(Integer duracionBateria) {
        this.duracionBateria = duracionBateria;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "tipo_bateria", referencedColumnName = "id")
    public DetalleCatalogo getTipoBateria() {
        return tipoBateria;
    }

    public void setTipoBateria(DetalleCatalogo tipoBateria) {
        this.tipoBateria = tipoBateria;
    }

    @Column(name = "precio", nullable = true)
    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;
        if (o == null || getClass() != o.getClass())
            return false;

        Equipos equipos = (Equipos) o;

        if (id != null ? !id.equals(equipos.id) : equipos.id != null)
            return false;
        if (tipoProtesis != null ? !tipoProtesis.equals(equipos.tipoProtesis) : equipos.tipoProtesis != null)
            return false;
        if (modelo != null ? !modelo.equals(equipos.modelo) : equipos.modelo != null)
            return false;
        if (descripcion != null ? !descripcion.equals(equipos.descripcion) : equipos.descripcion != null)
            return false;
        if (numeroBaterias != null ? !numeroBaterias.equals(equipos.numeroBaterias) : equipos.numeroBaterias != null)
            return false;
        if (duracionBateria != null ? !duracionBateria.equals(equipos.duracionBateria)
                : equipos.duracionBateria != null)
            return false;
        if (tipoBateria != null ? !tipoBateria.equals(equipos.tipoBateria) : equipos.tipoBateria != null)
            return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (tipoProtesis != null ? tipoProtesis.hashCode() : 0);
        result = 31 * result + (modelo != null ? modelo.hashCode() : 0);
        result = 31 * result + (descripcion != null ? descripcion.hashCode() : 0);
        result = 31 * result + (numeroBaterias != null ? numeroBaterias.hashCode() : 0);
        result = 31 * result + (duracionBateria != null ? duracionBateria.hashCode() : 0);
        result = 31 * result + (tipoBateria != null ? tipoBateria.hashCode() : 0);
        return result;
    }
}
