package ni.com.audinic.businesslogic.service;

import ni.com.audinic.model.catalogos.Catalogo;
import ni.com.audinic.model.catalogos.DetalleCatalogo;
import ni.com.audinic.repository.dao.DAOException;

import java.util.List;

/**
 * Creado por fmgarcia el 19 / 06 / 2017.
 */
public interface DetalleCatalogoService {

    void agregar(DetalleCatalogo detalleCatalogo)throws DAOException;

    void modificar(DetalleCatalogo detalleCatalogo)throws DAOException;

    List<DetalleCatalogo> listarCatalogos();

    DetalleCatalogo encontrarPorCodigo(String padreCodigo,String codigo);

    DetalleCatalogo encontrarPorCodigo(String codigo);

    List<DetalleCatalogo> listarPorCodigo(String codigo);

}
