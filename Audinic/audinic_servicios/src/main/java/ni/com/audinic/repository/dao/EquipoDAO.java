package ni.com.audinic.repository.dao;

import ni.com.audinic.model.catalogos.Equipos;

public interface EquipoDAO extends GenericDAO<Equipos, Integer> {

}
