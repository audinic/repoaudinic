package ni.com.audinic.repository.dao;

import ni.com.audinic.model.catalogos.Departamento;

public interface DepartamentoDAO extends GenericDAO<Departamento, Integer> {

}
