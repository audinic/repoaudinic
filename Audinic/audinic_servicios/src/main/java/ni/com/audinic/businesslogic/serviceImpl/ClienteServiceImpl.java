package ni.com.audinic.businesslogic.serviceImpl;

import java.util.List;

import ni.com.audinic.repository.dao.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.Sort;

import ni.com.audinic.businesslogic.service.ClienteService;
import ni.com.audinic.model.catalogos.Cliente;
import ni.com.audinic.repository.dao.ClienteDAO;

@Service
@Transactional
public class ClienteServiceImpl implements ClienteService {

    @Autowired
    ClienteDAO clienteDAO;

    @Override
    public void agregarCliente(Cliente cliente) throws DAOException {
        clienteDAO.saveUpper(cliente);
    }

    @Override
    public void modificarCliente(Cliente cliente) throws DAOException{
        clienteDAO.updateUpper(cliente);
    }

    @Override
    public boolean eliminarCliente(Cliente cliente) {
        return clienteDAO.remove(cliente);
    }

    @Override
    public List<Cliente> listarClientes() {
        Search search = new Search();
        search.addFetches("categoria","sexo");
        search.addSortAsc("primerNombre");
        search.addSortAsc("primerApellido");
        return clienteDAO.search(search);
    }

    @Override
    public Cliente obtenerClientePorId(Integer id) {
        Search search = new Search();
        search.addFilterEqual("id", id);
        return clienteDAO.searchUnique(search);
    }

}
