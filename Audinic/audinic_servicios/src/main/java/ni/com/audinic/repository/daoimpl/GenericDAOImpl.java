package ni.com.audinic.repository.daoimpl;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.SearchResult;
import com.googlecode.genericdao.search.hibernate.HibernateMetadataUtil;
import com.googlecode.genericdao.search.hibernate.HibernateSearchProcessor;
import ni.com.audinic.repository.dao.DAOException;
import ni.com.audinic.repository.dao.GenericDAO;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import javax.annotation.PostConstruct;
import javax.persistence.TypedQuery;
import java.io.Serializable;
import java.lang.reflect.*;
import java.util.*;

/**
 * Creado por fmgarcia el 15 / 06 / 2017.
 */

public class GenericDAOImpl<T, ID extends Serializable> implements GenericDAO<T, ID> {
    Logger logger = Logger.getLogger(GenericDAOImpl.class);
    
    @Autowired
    public SessionFactory sessionFactory;
    private Class<T> entityClass;
    private HibernateSearchProcessor searchProcessor;
    private HibernateMetadataUtil metadataUtil;

    public GenericDAOImpl() {
        ParameterizedType genericSuperclass = (ParameterizedType) this.getClass().getGenericSuperclass();
        this.entityClass = (Class) genericSuperclass.getActualTypeArguments()[0];
    }

    @PostConstruct
    public void init() {
        this.searchProcessor = HibernateSearchProcessor.getInstanceForSessionFactory(this.sessionFactory);
        this.metadataUtil = HibernateMetadataUtil.getInstanceForSessionFactory(this.sessionFactory);
    }

    public void save(Object entity) throws DAOException{
        try {
            this.getSession().save(entity);
        } catch (Exception var3) {
            this.logger.error(var3);
            throw new DAOException(var3);
        }
    }

    public void saveUpper(Object entity) throws DAOException{
        try {
            this.toUpperCaseTrim(entity);
            this.getSession().save(entity);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException var3) {
            this.logger.error(var3);
            throw new DAOException(var3);
        }
    }

    public void updateUpper(Object entity) throws DAOException{
        try {
            this.toUpperCaseTrim(entity);
            this.getSession().merge(entity);
        } catch (InvocationTargetException | IllegalAccessException | NoSuchMethodException var3) {
            this.logger.error(var3);
            throw new DAOException(var3);
        }
    }

    public void update(Object entity) throws DAOException{
        try {
            this.toTrim(entity);
            this.getSession().merge(entity);
        } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException var3) {
            this.logger.error(var3);
            throw new DAOException(var3);
        }
    }

    public boolean remove(Object entity) {
        if (entity != null) {
            this.getSession().delete(entity);
            return true;
        } else {
            return false;
        }
    }

    public void remove(Object... entities) {
        Object[] var2 = entities;
        int var3 = entities.length;

        for (int var4 = 0; var4 < var3; ++var4) {
            Object entity = var2[var4];
            if (entity != null) {
                this.remove(entity);
            }
        }

    }

    public int count(ISearch search) {
        if(search == null) {
            throw new NullPointerException("Search is null.");
        } else if(search.getSearchClass() != null && !search.getSearchClass().equals(this.entityClass)) {
            throw new IllegalArgumentException("Search class does not match expected type: " + this.entityClass.getName());
        } else {
            return this.searchProcessor.count(this.getSession(), this.entityClass, search);
        }
    }

    @SuppressWarnings("unchecked")
    public T searchUnique(ISearch search) {
        if (search == null) {
            throw new NullPointerException("Search is null.");
        } else if (search.getSearchClass() != null && !search.getSearchClass().equals(this.entityClass)) {
            throw new IllegalArgumentException(
                    "Search class does not match expected type: " + this.entityClass.getName());
        } else {
            return (T) this.searchProcessor.searchUnique(this.getSession(), this.entityClass, search);
        }
    }

    @SuppressWarnings("unchecked")
    public List<T> search(ISearch search) {
        if (search == null) {
            throw new NullPointerException("Search is null.");
        } else if (search.getSearchClass() != null && !search.getSearchClass().equals(this.entityClass)) {
            throw new IllegalArgumentException(
                    "Search class does not match expected type: " + this.entityClass.getName());
        } else {
            return (List<T>) this.searchProcessor.search(this.getSession(), this.entityClass, search);
        }
    }

    public SearchResult lazySearch(int first, int pageSize, String sortField, String sortOrder, Map<String, Object> filters) {
        Object list = null;
        boolean totalCount = false;
        SearchResult searchResult = new SearchResult();
        Search search = new Search(this.entityClass);
        if(filters != null && !filters.isEmpty()) {
            Iterator it = filters.keySet().iterator();

            while(it.hasNext()) {
                String filterProperty = (String)it.next();
                Object filterValue = filters.get(filterProperty);
                if(filterValue instanceof String) {
                    search.addFilterILike(filterProperty, "%" + filterValue.toString() + "%");
                } else {
                    search.addFilterEqual(filterProperty, filterValue);
                }
            }
        }

        int totalCount1 = this.searchProcessor.count(this.getSession(), search);
        searchResult.setTotalCount(totalCount1);
        if(totalCount1 > 0) {
            if(!StringUtils.isBlank(sortField) && !StringUtils.isBlank(sortOrder) && sortOrder.toLowerCase().contains("asc")) {
                search.addSortAsc(sortField);
            }

            search.setFirstResult(first);
            search.setMaxResults(pageSize);
            list = this.searchProcessor.search(this.getSession(), search);
        }

        if(list == null) {
            list = new ArrayList();
        }

        searchResult.setResult((List)list);
        return searchResult;
    }

    private void toTrim(Object entity) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Field[] fields = entity.getClass().getDeclaredFields();
        String methodList = this.getMethodsNameByEntity(entity);
        if (!methodList.isEmpty()) {
            Field[] var4 = fields;
            int var5 = fields.length;

            for (int var6 = 0; var6 < var5; ++var6) {
                Field f = var4[var6];
                String field = String.valueOf(f.getName().charAt(0)).toUpperCase() + f.getName().substring(1);
                if (f.getType() == String.class && methodList.contains("get" + field)
                        && methodList.contains("set" + field)) {
                    Method getter = entity.getClass().getDeclaredMethod("get" + field, new Class[0]);
                    Method setter = entity.getClass().getDeclaredMethod("set" + field, new Class[] { String.class });
                    Object value = getter.invoke(entity, new Object[0]);
                    if (value != null) {
                        setter.invoke(entity, new Object[] { ((String) value).trim() });
                    }
                }
            }
        }
    }

    private Session getSession() {
        return this.sessionFactory.getCurrentSession();
    }

    private String getMethodsNameByEntity(Object entity) {
        String methodList = "";
        Method[] methods = entity.getClass().getDeclaredMethods();
        if (methods != null) {
            methodList = Arrays.toString(methods);
        }
        return methodList;
    }

    private void toUpperCaseTrim(Object entity)
            throws NoSuchMethodException, IllegalAccessException, InvocationTargetException {
        Field[] fields = entity.getClass().getDeclaredFields();
        String methodList = this.getMethodsNameByEntity(entity);
        if (methodList != null) {
            Field[] var4 = fields;
            int var5 = fields.length;

            for (int var6 = 0; var6 < var5; ++var6) {
                Field f = var4[var6];
                String field = String.valueOf(f.getName().charAt(0)).toUpperCase() + f.getName().substring(1);
                if (f.getType() == String.class && methodList.contains("get" + field)
                        && methodList.contains("set" + field)) {
                    Method getter = entity.getClass().getDeclaredMethod("get" + field, new Class[0]);
                    Method setter = entity.getClass().getDeclaredMethod("set" + field, new Class[] { String.class });
                    Object value = getter.invoke(entity, new Object[0]);
                    if (value != null) {
                        setter.invoke(entity, new Object[] { ((String) value).trim().toUpperCase() });
                    }
                }
            }
        }
    }

}
