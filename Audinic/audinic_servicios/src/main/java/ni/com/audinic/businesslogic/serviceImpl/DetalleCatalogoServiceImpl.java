package ni.com.audinic.businesslogic.serviceImpl;

import com.googlecode.genericdao.search.Search;
import ni.com.audinic.businesslogic.service.DetalleCatalogoService;
import ni.com.audinic.model.catalogos.Catalogo;
import ni.com.audinic.model.catalogos.DetalleCatalogo;
import ni.com.audinic.repository.dao.DAOException;
import ni.com.audinic.repository.dao.DetalleCatalogoDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Creado por fmgarcia el 19 / 06 / 2017.
 */
@Service
@Transactional
public class DetalleCatalogoServiceImpl implements DetalleCatalogoService {

    @Autowired
    private DetalleCatalogoDAO detalleCatalogoDAO;

    @Override
    public void agregar(DetalleCatalogo detalleCatalogo) throws DAOException {
        detalleCatalogoDAO.saveUpper(detalleCatalogo);
    }

    @Override
    public void modificar(DetalleCatalogo detalleCatalogo)throws DAOException {
        detalleCatalogoDAO.updateUpper(detalleCatalogo);
    }

    public DetalleCatalogo encontrarPorCodigo(String padreCodigo, String codigo){
        Search search= new Search();
        search.addFilterEqual("codigo", codigo);
        search.addFilterEqual("catalogo.codigo", padreCodigo);

        return detalleCatalogoDAO.searchUnique(search);
    }

    public DetalleCatalogo encontrarPorCodigo(String codigo){
        Search search= new Search();
        search.addFilterEqual("codigo", codigo);

        return detalleCatalogoDAO.searchUnique(search);
    }

    @Override
    public List<DetalleCatalogo> listarCatalogos() {
     return  detalleCatalogoDAO.listarTodo();
    }

    @Override
    public List<DetalleCatalogo> listarPorCodigo(String codigo){
        Search search= new Search();
        search.addFilterEqual("catalogo.codigo",codigo);

        return (List<DetalleCatalogo>) detalleCatalogoDAO.search(search);
    }
}
