package ni.com.audinic.model.producto;

import ni.com.audinic.model.catalogos.Cliente;
import ni.com.audinic.model.catalogos.DetalleCatalogo;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;

/**
 * Creado por fmgarcia el 12 / 06 / 2017.
 */
@Entity
@Table(name = "solicitud",schema = "producto_audinic")
public class Solicitud implements Serializable{
    private static final long serialVersionUID = 1L;

    private Integer id;
    private Solicitud solicitud_padre;
    private Cliente cliente;
    private DetalleCatalogo origen;
    private String numeroSolicitud;
    private SituacionSolicitud situacion;
    private Date fechaCreado;
    private Date fechaGeneracion;
    private Date fechaRecepcion;
    private Date fechaVerificacion;
    private Date fechaEntrega;
    private Boolean cobrada;

    @Id
    @SequenceGenerator(name ="pk_solicitud" ,sequenceName = "producto_audinic.solicitud_id_seq",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "pk_solicitud")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "solicitud_padre", referencedColumnName = "id")
    public Solicitud getSolicitud_padre() {
        return solicitud_padre;
    }

    public void setSolicitud_padre(Solicitud solicitud_padre) {
        this.solicitud_padre = solicitud_padre;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cliente", referencedColumnName = "id")
    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "origen", referencedColumnName = "id")
    public DetalleCatalogo getOrigen() {
        return origen;
    }

    public void setOrigen(DetalleCatalogo origen) {
        this.origen = origen;
    }


    @Column(name = "numero_solicitud", nullable = true, length = 50)
    public String getNumeroSolicitud() {
        return numeroSolicitud;
    }

    public void setNumeroSolicitud(String numeroSolicitud) {
        this.numeroSolicitud = numeroSolicitud;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "situacion", nullable = false, length = 2)
    public SituacionSolicitud getSituacion() {
        return situacion;
    }

    public void setSituacion(SituacionSolicitud situacion) {
        this.situacion = situacion;
    }

    @Column(name = "fecha_creado", nullable = false)
    public Date getFechaCreado() {
        return fechaCreado;
    }

    public void setFechaCreado(Date fechaCreado) {
        this.fechaCreado = fechaCreado;
    }


    @Column(name = "fecha_generacion", nullable = true)
    public Date getFechaGeneracion() {
        return fechaGeneracion;
    }

    public void setFechaGeneracion(Date fechaGeneracion) {
        this.fechaGeneracion = fechaGeneracion;
    }


    @Column(name = "fecha_recepcion", nullable = true)
    public Date getFechaRecepcion() {
        return fechaRecepcion;
    }

    public void setFechaRecepcion(Date fechaRecepcion) {
        this.fechaRecepcion = fechaRecepcion;
    }


    @Column(name = "fecha_verificacion", nullable = true)
    public Date getFechaVerificacion() {
        return fechaVerificacion;
    }

    public void setFechaVerificacion(Date fechaVerificacion) {
        this.fechaVerificacion = fechaVerificacion;
    }


    @Column(name = "fecha_entrega", nullable = true)
    public Date getFechaEntrega() {
        return fechaEntrega;
    }

    public void setFechaEntrega(Date fechaEntrega) {
        this.fechaEntrega = fechaEntrega;
    }

    @Column(name = "cobrada", nullable = true)
    public Boolean getCobrada() {
        return cobrada;
    }

    public void setCobrada(Boolean cobrada) {
        this.cobrada = cobrada;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Solicitud solicitud = (Solicitud) o;

        if (id != null ? !id.equals(solicitud.id) : solicitud.id != null) return false;
        if (origen != null ? !origen.equals(solicitud.origen) : solicitud.origen != null) return false;
        if (numeroSolicitud != null ? !numeroSolicitud.equals(solicitud.numeroSolicitud) : solicitud.numeroSolicitud != null)
            return false;
        if (fechaCreado != null ? !fechaCreado.equals(solicitud.fechaCreado) : solicitud.fechaCreado != null)
            return false;
        if (fechaGeneracion != null ? !fechaGeneracion.equals(solicitud.fechaGeneracion) : solicitud.fechaGeneracion != null)
            return false;
        if (fechaRecepcion != null ? !fechaRecepcion.equals(solicitud.fechaRecepcion) : solicitud.fechaRecepcion != null)
            return false;
        if (fechaVerificacion != null ? !fechaVerificacion.equals(solicitud.fechaVerificacion) : solicitud.fechaVerificacion != null)
            return false;
        if (fechaEntrega != null ? !fechaEntrega.equals(solicitud.fechaEntrega) : solicitud.fechaEntrega != null)
            return false;
        if (cobrada != null ? !cobrada.equals(solicitud.cobrada) : solicitud.cobrada != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (origen != null ? origen.hashCode() : 0);
        result = 31 * result + (numeroSolicitud != null ? numeroSolicitud.hashCode() : 0);
        result = 31 * result + (fechaCreado != null ? fechaCreado.hashCode() : 0);
        result = 31 * result + (fechaGeneracion != null ? fechaGeneracion.hashCode() : 0);
        result = 31 * result + (fechaRecepcion != null ? fechaRecepcion.hashCode() : 0);
        result = 31 * result + (fechaVerificacion != null ? fechaVerificacion.hashCode() : 0);
        result = 31 * result + (fechaEntrega != null ? fechaEntrega.hashCode() : 0);
        result = 31 * result + (cobrada != null ? cobrada.hashCode() : 0);
        return result;
    }
}
