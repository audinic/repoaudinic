package ni.com.audinic.businesslogic.serviceImpl;

import com.googlecode.genericdao.search.Filter;
import com.googlecode.genericdao.search.Search;
import ni.com.audinic.businesslogic.service.CatalogoService;
import ni.com.audinic.model.catalogos.Catalogo;
import ni.com.audinic.repository.dao.CatalogoDAO;
import ni.com.audinic.repository.dao.DAOException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Creado por fmgarcia el 19 / 06 / 2017.
 */
@Service
@Transactional
public class CatalogoServiceImpl implements CatalogoService{

    @Autowired
    private CatalogoDAO catalogoDAO;

    @Override
    public void agregar(Catalogo catalogo) throws DAOException {
        catalogoDAO.saveUpper(catalogo);
    }

    @Override
    public void modificar(Catalogo catalogo) throws DAOException{
        catalogoDAO.updateUpper(catalogo);
    }

    @Override
    public List<Catalogo> listarCatalogos(final String filtro) {
        List<Catalogo> catalogos;
        if(StringUtils.isNotBlank(filtro))
        {

            Filter filtroCodigo = new Filter();
            filtroCodigo.setOperator(Filter.OP_ILIKE);
            filtroCodigo.setProperty("codigo");
            filtroCodigo.setValue("%" + filtro + "%");

            Filter filtroNombre = new Filter();
            filtroNombre.setOperator(Filter.OP_ILIKE);
            filtroNombre.setProperty("nombre");
            filtroNombre.setValue("%" + filtro + "%");

            Filter filtroDescripcion = new Filter();
            filtroDescripcion.setOperator(Filter.OP_ILIKE);
            filtroDescripcion.setProperty("descripcion");
            filtroDescripcion.setValue("%" + filtro + "%");

            Search oSearch = new Search();
            oSearch.addFilter(Filter.or(filtroNombre, filtroDescripcion, filtroCodigo));
            oSearch.addSortAsc("nombre");
            catalogos = catalogoDAO.search(oSearch);
        }else{
            catalogos=catalogoDAO.listarTodo();
        }
        return catalogos;
    }


}
