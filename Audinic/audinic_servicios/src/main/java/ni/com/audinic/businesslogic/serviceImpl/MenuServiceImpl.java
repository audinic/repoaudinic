package ni.com.audinic.businesslogic.serviceImpl;

import com.googlecode.genericdao.search.Search;
import com.googlecode.genericdao.search.Sort;
import ni.com.audinic.businesslogic.service.MenuService;
import ni.com.audinic.model.seguridad.Menu;
import ni.com.audinic.repository.dao.DAOException;
import ni.com.audinic.repository.dao.MenuDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Creado por fmgarcia el 15 / 06 / 2017.
 */
@Service
@Transactional
public class MenuServiceImpl implements MenuService {

    @Autowired
    MenuDAO menuDAO;

    @Override
    public void agregar(Menu menu) throws DAOException {
        menuDAO.saveUpper(menu);
    }

    @Override
    public List<Menu> listarMenus() {
        Search search = new Search();
        search.addFetch("padre");
        search.addSorts(Sort.asc("id"), Sort.asc("orden"));
        return menuDAO.search(search);
    }

    @Override
    public Menu encontrarPorId(Integer idMenu) {
        Search s = new Search();
        s.addFilterEqual("id", idMenu);

        return menuDAO.searchUnique(s);
    }
}
