package ni.com.audinic.repository.dao;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import com.googlecode.genericdao.search.ISearch;
import com.googlecode.genericdao.search.SearchResult;

/**
 * Creado por fmgarcia el 15 / 06 / 2017.
 */
public interface GenericDAO<T, ID extends Serializable> {
    
    void save(Object var1) throws DAOException;

    void saveUpper(Object var1)throws DAOException;

    void updateUpper(Object entity)throws DAOException;

    void update(Object entity)throws DAOException;

    boolean remove(Object entity);

    void remove(Object... entities);

    T searchUnique(ISearch search);

    List<T> search(ISearch search);

    int count(ISearch search);



}
