package ni.com.audinic.model.producto;

import ni.com.audinic.model.catalogos.Equipos;
import ni.com.audinic.model.catalogos.Servicio;

import javax.persistence.*;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Creado por fmgarcia el 12 / 06 / 2017.
 */
@Entity
@Table(name = "detalle_solicitud", schema = "producto_audinic")
public class DetalleSolicitud implements Serializable{

    private Integer id;
    private Solicitud solicitud;
    private Servicio servicio;
    private Equipos equipos;
    private Integer cantidad;
    private BigDecimal precio;

    @Id
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }


    public void setId(Integer id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Solicitud.class)
    @JoinColumn(name = "solicitud", referencedColumnName = "id")
    public Solicitud getSolicitud() {
        return solicitud;
    }

    public void setSolicitud(Solicitud solicitud) {
        this.solicitud = solicitud;
    }

    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Servicio.class)
    @JoinColumn(name = "id_servicio", referencedColumnName = "id")
    public Servicio getServicio() {
        return servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    @ManyToOne(fetch = FetchType.LAZY,targetEntity = Equipos.class)
    @JoinColumn(name = "id_equipo", referencedColumnName = "id")
    public Equipos getEquipos() {
        return equipos;
    }

    public void setEquipos(Equipos equipos) {
        this.equipos = equipos;
    }

    @Column(name = "cantidad", nullable = true)
    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(Integer cantidad) {
        this.cantidad = cantidad;
    }


    @Column(name = "precio", nullable = false, precision = 2)
    public BigDecimal getPrecio() {
        return precio;
    }

    public void setPrecio(BigDecimal precio) {
        this.precio = precio;
    }


}
