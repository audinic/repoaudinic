package ni.com.audinic.repository.daoimpl;

import org.springframework.stereotype.Repository;
import ni.com.audinic.model.catalogos.Departamento;
import ni.com.audinic.repository.dao.DepartamentoDAO;

@Repository
public class DepartamentoDAOImpl extends GenericDAOImpl<Departamento, Integer> implements DepartamentoDAO {

}
