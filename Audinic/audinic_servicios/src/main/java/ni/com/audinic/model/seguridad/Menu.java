package ni.com.audinic.model.seguridad;

import javax.persistence.*;

/**
 * Creado por fmgarcia el 15 / 06 / 2017.
 */
@Entity
@Table(name = "menu", schema = "seguridad")
public class Menu {
    private Integer id;
    private String nombre;
    private Menu padre;
    private String url;
    private Integer orden;
    private Boolean pasivo;


    @Id
    @SequenceGenerator(name = "pk_menu", sequenceName = "seguridad.menu_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "pk_menu")
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Column(name = "nombre", nullable = false, length = 100)
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @ManyToOne(fetch = FetchType.LAZY, targetEntity = Menu.class)
    @JoinColumn(name = "padre_menu", referencedColumnName = "id")
    public Menu getPadre() {
        return padre;
    }

    public void setPadre(Menu padre) {
        this.padre = padre;
    }

    @Column(name = "url", nullable = true, length = 1000)
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Column(name = "orden", nullable = false)
    public Integer getOrden() {
        return orden;
    }

    public void setOrden(Integer orden) {
        this.orden = orden;
    }

    @Column(name = "pasivo", nullable = true)
    public Boolean getPasivo() {
        return pasivo;
    }

    public void setPasivo(Boolean pasivo) {
        this.pasivo = pasivo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Menu menu = (Menu) o;

        if (id != null ? !id.equals(menu.id) : menu.id != null) return false;
        if (nombre != null ? !nombre.equals(menu.nombre) : menu.nombre != null) return false;
        if (url != null ? !url.equals(menu.url) : menu.url != null) return false;
        if (orden != null ? !orden.equals(menu.orden) : menu.orden != null) return false;
        if (pasivo != null ? !pasivo.equals(menu.pasivo) : menu.pasivo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (nombre != null ? nombre.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (orden != null ? orden.hashCode() : 0);
        result = 31 * result + (pasivo != null ? pasivo.hashCode() : 0);
        return result;
    }
}
