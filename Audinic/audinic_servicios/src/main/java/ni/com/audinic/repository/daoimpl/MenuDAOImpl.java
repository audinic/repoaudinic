package ni.com.audinic.repository.daoimpl;

import ni.com.audinic.model.seguridad.Menu;
import ni.com.audinic.repository.dao.MenuDAO;
import org.springframework.stereotype.Repository;

/**
 * Creado por fmgarcia el 15 / 06 / 2017.
 */
@Repository
public class MenuDAOImpl extends GenericDAOImpl<Menu,Integer> implements MenuDAO{

}
