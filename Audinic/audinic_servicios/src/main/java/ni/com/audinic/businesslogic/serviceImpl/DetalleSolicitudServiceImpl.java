package ni.com.audinic.businesslogic.serviceImpl;

import java.util.List;

import ni.com.audinic.repository.dao.DAOException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.googlecode.genericdao.search.Search;

import ni.com.audinic.businesslogic.service.DetalleSolicitudService;
import ni.com.audinic.model.producto.DetalleSolicitud;
import ni.com.audinic.repository.dao.DetalleSolicitudDAO;

@Service
@Transactional
public class DetalleSolicitudServiceImpl implements DetalleSolicitudService {

    @Autowired
    DetalleSolicitudDAO detalleSolicitudDAO;
    
    @Override
    public void agregarDetalleSolicitud(DetalleSolicitud detalleSolicitud) throws DAOException {
        detalleSolicitudDAO.saveUpper(detalleSolicitud);
    }

    @Override
    public void ModificarDetalleSolicitud(DetalleSolicitud detalleSolicitud) throws DAOException{
        detalleSolicitudDAO.updateUpper(detalleSolicitud);
    }

    @Override
    public boolean eliminarDetalleSolicitud(DetalleSolicitud detalleSolicitud) {
        return detalleSolicitudDAO.remove(detalleSolicitud);
    }

    @Override
    public List<DetalleSolicitud> listarDetalleSolicitudesPorIdSolicitud(Integer id) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public DetalleSolicitud obtenerDetalleSolicitudPorId(Integer id) {
        Search search = new Search();
        search.addFilterEqual("id", id);
        return detalleSolicitudDAO.searchUnique(search);
    }

}
